//
//  Constants.h
//  CarryTruckApp
//
//  Created by Sumit Sharma on 12/01/17.
//  Copyright © 2017 Kipl. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#pragma mark - Importing Modules
@import ImageIO;
@import CoreLocation;


#pragma mark - Importing Classes
#import "AppDelegate.h"
#import "UtilityClass.h"
#import "AppWebHandler.h"

#import "UIStoryboard+References.h"



#pragma mark - Importing 3rd Party Classes
//#import <GKActionSheetPicker/GKActionSheetPicker.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

#import "KiplToastView.h"


#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
//#import "DBManager.h"


#pragma mark - Header Keys
static NSString *const HEADER_LANG  =   @"en";
static NSString *const HEADER_VERSION  =   @"1.0";
static NSString *const HEADER_DEVICE  =   @"ios";

#define SERVICE_KEY    @"pLU7ikdVrUk6AH7ejfc6pPJCtBFecqKP4yBGWqzIenppqwVDzEMCBbHcuf3pSgPhJp"

#pragma mark - Web Service Macros
#define SERVICE_HOST    @"http://hamiltonpropertygroup.com.au/"
//#define API             SERVICE_HOST    @":9106/"

/////For Client Distribution  ----- http://202.157.76.19:9100/
//#define SERVICE_HOST    @"http://hamiltonpropertygroup.com.au/"
//#define API             SERVICE_HOST    @":9100/"


#pragma mark - Api(s)
static NSString *const API_PROPERTIE_LIST   = SERVICE_HOST       @"web/loginPropertyList.php";
static NSString *const API_SHORTLIST_LIST   = SERVICE_HOST       @"web/loginLikeList.php";
static NSString *const API_FILTER_LIST   = SERVICE_HOST       @"web/loginallfilters.php";
static NSString *const API_LOGIN    = SERVICE_HOST       @"web/login.php";
static NSString *const API_LIKE    = SERVICE_HOST       @"web/loginLike.php";
static NSString *const API_UNLIKE    = SERVICE_HOST       @"web/loginUnlike.php";
static NSString *const API_PROPERTIE_FILTER    = SERVICE_HOST       @"web/loginPropertyFilterList.php";
static NSString *const API_GET_ADMIN_DETAIL    = SERVICE_HOST       @"web/loginadminapi.php";
static NSString *const API_GET_OFFER_LIST    = SERVICE_HOST       @"web/loginofferlist.php";
static NSString *const API_FORGOT_PASSWORD    = SERVICE_HOST       @"web/loginforgotpass.php";



#pragma mark - Application Macro(s)
#define APP_NAME            @"Hamilton Properties"
#define GLOBAL_ERROR        @"We can not process your request right now, \n Please try again!"

#pragma mark - Google Api
static NSString *const Google_Api_Key   =   @"AIzaSyAv8oSh8tBKzmXayTZ_JrW1SMZG2f1NOdQ";



#endif /* Constants_h */
