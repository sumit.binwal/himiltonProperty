//
//  KiplToastView.h
//  Philtr 'em
//
//  Created by Bharat Kumar Pathak on 19/08/16.
//  Copyright © 2016 Vaibhav Khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KiplToastView : UIView

+(void)showToastOver:(UIView *)view message:(NSString *)messageToShow duration:(CGFloat)showDuration;

@end
