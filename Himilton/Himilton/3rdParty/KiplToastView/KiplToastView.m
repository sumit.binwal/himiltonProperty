//
//  KiplToastView.m
//  Philtr 'em
//
//  Created by Bharat Kumar Pathak on 19/08/16.
//  Copyright © 2016 Vaibhav Khatri. All rights reserved.
//

#import "KiplToastView.h"

@interface KiplToastView ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@end

@implementation KiplToastView

-(void)awakeFromNib{
    [super awakeFromNib];
}

+(void)showToastOver:(UIView *)view message:(NSString *)messageToShow duration:(CGFloat)showDuration {
    
    CGFloat scaleFactorX = [UIScreen mainScreen].bounds.size.width/375;
    CGFloat scaleFactorY = [UIScreen mainScreen].bounds.size.height/667;
    
    UIView *existingToast = [view viewWithTag:500];
    if(existingToast){
        [existingToast removeFromSuperview];
    }
    
    KiplToastView *toastView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([KiplToastView class]) owner:self options:nil][0];
    toastView.frame = view.frame;
    
    toastView.messageLabel.font = [UIFont fontWithName:toastView.messageLabel.font.fontName size:toastView.messageLabel.font.pointSize*scaleFactorX];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = toastView.messageLabel.font.lineHeight + 2;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSDictionary *attributes = @{NSFontAttributeName:toastView.messageLabel.font,
                                 NSForegroundColorAttributeName:[UIColor whiteColor],
                                 NSBackgroundColorAttributeName:[UIColor clearColor],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 };
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:messageToShow
                                           attributes:attributes];
    
    [toastView.messageLabel setAttributedText:attributedText];
    
    toastView.labelWidthConstraint.constant = toastView.labelWidthConstraint.constant  * scaleFactorX;
    toastView.labelHeightConstraint.constant = toastView.labelHeightConstraint.constant  * scaleFactorY;
    [toastView setAlpha:0.0f];
    [view addSubview:toastView];
    [view bringSubviewToFront:toastView];
    
    [UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [toastView setAlpha: 1.0f];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5f delay:showDuration options:UIViewAnimationOptionCurveEaseOut animations:^{
            [toastView setAlpha: 0.0f];
        } completion:^(BOOL finished) {
            [toastView removeFromSuperview];
        }];
    }];
}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        //statements
//    }
//    return self;
//}

//+(void)showToastOver:(UIView *)view message:(NSString *)messageToShow duration:(CGFloat)showDuration {
//    
//    @autoreleasepool {
//        CGRect initialFrame = CGRectMake(0, 0, view.frame.size.width-50, 0);
//        
//        UILabel *messageLabel = [[UILabel alloc] initWithFrame:initialFrame];
//        [messageLabel setTextColor:[UIColor whiteColor]];
//        [messageLabel setTextAlignment:NSTextAlignmentCenter];
//        [messageLabel setNumberOfLines:0];
//        [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
//        [messageLabel setFont:AppFont(16)];
//        [messageLabel setText:messageToShow];
//        
//        NSLog(@"::::::%f", messageLabel.intrinsicContentSize.width);
//        CGFloat messageHeight = [self getLabelHeight:messageLabel];
//        CGRect computedFrame = CGRectMake(20, view.frame.size.height-messageHeight-40, messageLabel.intrinsicContentSize.width, messageHeight+10);
//        
//        KiplToastView *toastView = [[KiplToastView alloc] initWithFrame:CGRectMake(computedFrame.origin.x, computedFrame.origin.y, computedFrame.size.width+computedFrame.origin.x*2, computedFrame.size.height)];
//        [toastView setBackgroundColor:[UIColor blackColor]];
//        [toastView.layer setCornerRadius:5.0f];
//        [toastView setAlpha:0.0f];
//        
//        messageLabel.frame = CGRectMake(5, 0, toastView.frame.size.width+10, toastView.frame.size.height);
//        [toastView addSubview:messageLabel];
//        [view addSubview:toastView];
//        
//        [UIView animateWithDuration:1.0f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//            [toastView setAlpha: 1.0f];
//        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:1.0f delay:showDuration options:UIViewAnimationOptionCurveEaseOut animations:^{
//                [toastView setAlpha: 0.0f];
//            } completion:^(BOOL finished) {
//                [toastView removeFromSuperview];
//            }];
//        }];
//    }
//}

//+ (CGFloat)getLabelHeight:(UILabel*)label
//{
//    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
//    CGSize size;
//    
//    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
//    CGSize boundingBox = [label.text boundingRectWithSize:constraint
//                                                  options:NSStringDrawingUsesLineFragmentOrigin
//                                               attributes:@{NSFontAttributeName:label.font}
//                                                  context:context].size;
//    
//    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
//    
//    return size.height;
//}

@end
