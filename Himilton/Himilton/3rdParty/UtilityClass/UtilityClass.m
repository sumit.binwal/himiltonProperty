//
//  UtilityClass.m
//  Tabco App
//
//  Created by Santosh on 17/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import "UtilityClass.h"
#import "MBHUDView.h"
#pragma mark -
#pragma mark - CLASS IMPLEMENTATION
@implementation UtilityClass

#pragma mark -
#pragma mark- Custom method for check null value of string
+(BOOL)checkNullString:(NSString *)str
{
    if (str==nil || str==(id)[NSNull null] || [str isEqualToString:@"(null)"] || [str isEqualToString:@""] || [str isEqualToString:@"<null>"] || [str isEqualToString:@"null"])
        return NO;
    
    return YES;
}
#pragma mark -
#pragma mark - Date to String conversion
+(NSString*)date2str:(NSDate*)myNSDateInstance withFormat:(NSString *)strFormat{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:strFormat];
    formatter.AMSymbol = @"am";
    formatter.PMSymbol = @"pm";
    
    //Optionally for time zone conversions
    //   [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    
    NSString *stringFromDate = [formatter stringFromDate:myNSDateInstance];
    return stringFromDate;
}


#pragma mark -
#pragma mark - Add Top Border To A View
+(void)addTopBorder:(UIView *)view rgb:(UIColor*)rgb
{
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = rgb.CGColor;
    sublayer.frame = CGRectMake(0, view.bounds.size.height*scaleFactorX-1, view.frame.size.width*scaleFactorX, 1);
    [view.layer addSublayer:sublayer];
}

#pragma mark -
#pragma mark - Add Bottom Border To A View
+(void)addBottomBorder:(UIView *)view rgb:(UIColor*)rgb
{
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = rgb.CGColor;
    sublayer.frame = CGRectMake(0, 0, view.frame.size.width*scaleFactorX, 1);
    [view.layer addSublayer:sublayer];
}



#pragma mark -
#pragma mark String To Image
+ (UIImage *)convertBase64StringToImage:(NSString *)base64String
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:base64String options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *image = [UIImage imageWithData:data];
    return image;
}


#pragma mark -
#pragma mark Set Navigation Bar
+ (void)setNavigationBar:(UINavigationController *)navController
{
    [navController.navigationBar setTintColor:[UIColor whiteColor]];
    [navController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                 [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                 nil]];
    [navController.navigationBar setBarTintColor:[UIColor colorWithRed:(246.0/255.0) green:(124.0/255.0) blue:(35.0/255.0) alpha:1.0f]];
}


#pragma mark - User Login Token
+ (NSString *)getUserInfoLoginTokenValue
{
    NSDictionary *dictionary = [UtilityClass getUserInfoDictionary];
    
     NSString *loginToken = dictionary[@"userId"];
    
    return loginToken;
}

#pragma mark -
#pragma mark Show Hud
+ (void)showHud
{
    [MBHUDView hudWithBody:@"Wait." type:MBAlertViewHUDTypeActivityIndicator hidesAfter:-1 show:YES];
}

+ (void)hideHud
{
    [MBHUDView dismissCurrentHUD];
}
#pragma mark -
#pragma mark : Network Indicator Hud Show/Hide Method
+ (void)showNetworkIndicator
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
}

+ (void)hideNetworkIndicator
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
}

#pragma mark -
#pragma mark Image To String
+ (NSString *)convertImageToBase64String:(UIImage *)image
{
    NSData *data = UIImageJPEGRepresentation(image, .5);
    NSString *base64String = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return base64String;
}

#pragma mark -
+ (NSData *)convertDictionaryToData:(NSDictionary *)dictionary
{
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
//    NSString *str = [[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding];
    if (error)
    {
        return nil;
    }
    return postData;
}

#pragma mark -
#pragma mark UIAlertView
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock
{
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        for (NSInteger i=0; i<buttonsArray.count; i++)
        {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:buttonsArray[i] style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           if (dismissBlock)
                                                           {
                                                               dismissBlock(i);
                                                           }
                                                           [alertController dismissViewControllerAnimated:YES completion:^{
                                                           }];
                                                           
                                                       }];
            
            [alertController addAction:ok];
        }
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
}

+ (void)showSheetWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock
{
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSInteger i=0; i<buttonsArray.count; i++)
        {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:buttonsArray[i] style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           if (dismissBlock)
                                                           {
                                                               dismissBlock(i);
                                                           }
                                                           [alertController dismissViewControllerAnimated:YES completion:^{
                                                           }];
                                                           
                                                       }];
            
            [alertController addAction:ok];
        }
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
}

+ (void)showEmailFieldAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex,NSArray *fieldsArray))dismissBlock
{
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        for (NSInteger i=0; i<buttonsArray.count; i++)
        {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:buttonsArray[i] style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           if (dismissBlock)
                                                           {
                                                               dismissBlock(i,alertController.textFields);
                                                           }
                                                           [alertController dismissViewControllerAnimated:YES completion:^{
                                                           }];
                                                           
                                                       }];
            
            [alertController addAction:ok];
        }
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"Enter email";
        }];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark Save Vehicle Info
+ (void)saveVehicleInfoWithDictionary:(NSDictionary *)dictionary
{
    NSData *userInfoData = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    
    [userDefaults setObject:userInfoData forKey:@"vehicleInfo"];
}
#pragma mark -
#pragma mark Get User Info
+ (NSDictionary *)getVehicleInfoDictionary
{
    NSData *userInfoData = [userDefaults objectForKey:@"vehicleInfo"];
    if (!userInfoData) {
        return nil;
    }
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
    if (!dict) {
        return nil;
    }
    return dict;
}



#pragma mark -
#pragma mark Save User Info
+ (void)saveUserInfoWithDictionary:(NSDictionary *)dictionary
{
    NSData *userInfoData = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    
    [userDefaults setObject:userInfoData forKey:@"userInfoDict"];
}

#pragma mark -
#pragma mark Get User Info
+ (NSDictionary *)getUserInfoDictionary
{
    NSData *userInfoData = [userDefaults objectForKey:@"userInfoDict"];
    if (!userInfoData) {
        return nil;
    }
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
    if (!dict) {
        return nil;
    }
    return dict;
}

#pragma mark -
#pragma mark Delete User Info
+ (void)deleteUserInfoDictionary
{
    [userDefaults removeObjectForKey:@"userInfoDict"];
    [userDefaults removeObjectForKey:@"vehicleInfo"];
    [userDefaults removeObjectForKey:@"userImageUrl"];
}


#pragma mark -
+ (NSString *)getUserInfoFirstNameValue
{
    NSString *firstName = @"";
    NSDictionary *userInfoDict = [UtilityClass getUserInfoDictionary];
    if (userInfoDict[@"first_name"])
    {
        firstName = userInfoDict[@"first_name"];
    }
    return firstName;
}
#pragma mark -
+ (NSString *)getUserInfoLastNameValue
{
    NSString *lastName = @"";
    NSDictionary *userInfoDict = [UtilityClass getUserInfoDictionary];
    if (userInfoDict[@"last_name"])
    {
        lastName = userInfoDict[@"last_name"];
    }
    return lastName;
}

#pragma mark -
#pragma mark : Draw Image View In Circle With Image Border Color And Width

+ (void)drawImageViewCircle:(UIImageView *)imgVw imageBorderWidth:(float)borderWidth borderColor:(UIColor *)borderColor
{
    imgVw.layer.cornerRadius=imgVw.layer.frame.size.width/2*scaleFactorX;
    imgVw.clipsToBounds=YES;
    imgVw.layer.borderColor=borderColor.CGColor;
    imgVw.layer.borderWidth=borderWidth;
}


+ (NSString *)getUserInfoImagesBasePath
{
    NSDictionary *dictionary = [UtilityClass getUserInfoDictionary];
    NSString *basePath = dictionary[@"user_image_base_path"];
    return basePath;
}


+ (NSString *)getUserInfoEmailValue
{
    NSDictionary *dictionary = [UtilityClass getUserInfoDictionary];
    return dictionary[@"user_email"]?dictionary[@"user_email"]:@"";
}

#pragma mark -
#pragma mark Image Bytes To Array
+(NSMutableArray *)convertImageBytesToArrayUsingImage:(UIImage *)image
{
    NSData *data = UIImageJPEGRepresentation(image, .3);
    const unsigned char *bytes = [data bytes]; // no need to copy the data
    NSUInteger length = [data length];
    NSMutableArray *byteArray = [NSMutableArray array];
    for (NSUInteger i = 0; i < length; i++) {
        [byteArray addObject:[NSNumber numberWithUnsignedChar:bytes[i]]];
    }
    return byteArray;
}


#pragma mark -
#pragma mark Validate Empty String
+ (BOOL)isValueNotEmpty:(NSString*)aString{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    aString = [aString stringByTrimmingCharactersInSet:whitespace];
    
    if (aString == nil || [aString length] == 0){
        
        return NO;
    }
    return YES;
}

#pragma mark -
#pragma mark Validate Mobile Number
+ (BOOL)isValidMobileNumber:(NSString*)number
{
    
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:number];
    
    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}

#pragma mark -
#pragma mark Trim Space In String
+(NSString *)trimSpaceInString:(NSString *)mainstr
{
    NSCharacterSet *whitespace=[NSCharacterSet whitespaceAndNewlineCharacterSet];
    mainstr=[mainstr stringByTrimmingCharactersInSet:whitespace];
    mainstr=[mainstr stringByReplacingOccurrencesOfString:@"  " withString:@""];
    return mainstr;
}



#pragma mark -
#pragma mark Email Validation
+(BOOL)isValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark -
#pragma mark Resize Image
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage *)cropImage:(UIImage *)image toResolution:(CGFloat)resolution
{
    CGFloat scaleFactor = 1;
    if (image.size.width>image.size.height) {
        scaleFactor = image.size.width/image.size.height;
    }
    else if (image.size.height>image.size.width) {
        scaleFactor = image.size.height/image.size.width;
    }
    
    CFDictionaryRef options = (__bridge CFDictionaryRef) @{
                                                           (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                           (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                           (id) kCGImageSourceThumbnailMaxPixelSize : [NSNumber numberWithFloat:resolution * scaleFactor],
                                                           (id) kCGImageSourceShouldCache : [NSNumber numberWithBool:false]
                                                           };
    
    CGImageSourceRef src = CGImageSourceCreateWithData((CFDataRef)UIImageJPEGRepresentation(image, 1.0f), NULL);
    
    CGImageRef ref = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
    
    UIImage *newImage = [UIImage imageWithCGImage:ref];
    
    CGImageRelease(ref);
    CFRelease(src);
    
    //
    UIGraphicsBeginImageContextWithOptions(newImage.size, NO, 0.0);
    
    [image drawInRect:CGRectMake(0, 0, newImage.size.width, newImage.size.height)];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIView *)SnapShot:(UIView *)theView {
    if ([theView respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
        return [theView snapshotViewAfterScreenUpdates:YES];
    } else {
        UIGraphicsBeginImageContextWithOptions(theView.bounds.size, theView.isOpaque, 0.0f);
        [theView.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return [[UIImageView alloc] initWithImage:image];
    }
}

+(BOOL)isUpdateOrLoginRequired:(NSDictionary *)dictionary fromViewController:(UIViewController *)viewVontroller{
    
    if([[dictionary valueForKey:@"hasUpdate"] intValue] == 1){
        
        [UtilityClass showAlertWithTitle:@"Philtr" message:@"Some new feature is added\nPlease update app" onViewController:viewVontroller withButtonsArray:@[@"OK"] dismissBlock:nil];
        return YES;
        
    } else if([[dictionary valueForKey:@"requireLogin"] intValue] == 1) {
        
        [UtilityClass showAlertWithTitle:@"Philtr" message:@"Some feature is modified\nPlease login again" onViewController:viewVontroller withButtonsArray:@[@"OK"] dismissBlock:nil];
        return YES;
        
    }
    return NO;
}

+(void)addGlobalBackgroundOn:(UIView *)view {
    UIImageView *backImageView = [[UIImageView alloc] initWithFrame:view.bounds];
    backImageView.image = [UIImage imageNamed:@"bg_global"];
    backImageView.tag = 1100;
    [view addSubview:backImageView];
    [view sendSubviewToBack:backImageView];
}

+(void)removeGlobalBackgroundFrom:(UIView *)view {
    UIImageView *background = [view viewWithTag:1100];
    [background removeFromSuperview];
}

+(void)clearDefaultsAndCache {
    [self deleteUserInfoDictionary];
//    [[SDImageCache sharedImageCache]clearMemory];
//    [[SDImageCache sharedImageCache]clearDisk];
}

+(BOOL)isAgeValidWith:(NSString *)birthdate {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *dateFromString = nil;
    // voila!
    dateFromString = [dateFormatter dateFromString:birthdate];
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:dateFromString
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    
    NSLog(@"birthdate :: %@", birthdate);
    NSLog(@"dateFromString :: %@", dateFromString);
    NSLog(@"calculated age :: %ld", (long)age);
    
    return age>=14;
}

+(CGFloat)heightOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize
{
    CGSize sizeOfText = [aString boundingRectWithSize: aSize
                                              options: NSStringDrawingUsesLineFragmentOrigin
                                           attributes: [NSDictionary dictionaryWithObject:aFont
                                                                                   forKey:NSFontAttributeName]
                                              context: nil].size;
    
    return ceilf(sizeOfText.height);
}

+ (NSString *)getFormattedStringUsingDateString:(NSString *)dateString
{
    @autoreleasepool {
        NSString *formattedString = nil;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [formatter setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss.SSSZ"];
        NSDate *date = [formatter dateFromString:dateString];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        formattedString = [formatter stringFromDate:date];
        return [formattedString copy];
    }
}

#pragma mark -
+(NSString *)convertTimeStampToString:(double)timeStamp
{
    NSString *string = nil;
//    string = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:timeStamp]];
    return string;
}

+(NSString *) convertTimeStampToDate :(NSDate *) date {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    [formatter setDateFormat:@"MMM dd, E"];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}




#pragma mark - Methods related to chat

+(NSString *)convertEmojiToUnicode:(NSString *)str_sendMessage{
    
    NSData *data = [str_sendMessage dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *return_unicodeStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    return return_unicodeStr;
}

+(NSString *)convertUnicodeToEmoji:(NSString *)str_receiveMessage{
    
    NSData *data1 = [str_receiveMessage dataUsingEncoding:NSUTF8StringEncoding];
    NSString *return_emojiStr = [[NSString alloc] initWithData:data1 encoding:NSNonLossyASCIIStringEncoding];
    
    return return_emojiStr;
}

+(NSString *)convertDictionaryToJsonString:(NSDictionary *)dict
{
    if (dict)
    {
        NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonStr = [[NSString alloc] initWithData:data
                                                  encoding:NSUTF8StringEncoding];
        
        return jsonStr;
    }
    return nil;
}

+(NSDictionary *)convertJsonStrToDictionary:(NSString *)jsonStr
{
    if (jsonStr) {
        NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error = nil;
        NSDictionary *myDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        return myDictionary;
    }
    return nil;
}


+(UIViewController *)existsViewController:(Class)viewControllerClass inside:(UINavigationController *)navigationController{
    
    for (UIViewController *oneVC in navigationController.viewControllers) {
        
        if ([oneVC isKindOfClass: viewControllerClass]) {
            return oneVC;
        }
    }
    return nil;
}


+(UIBarButtonItem *)getMenuBarButtonIcon
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"menuIcon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:nil action:nil];
    return buttonItem;
}

+(UIBarButtonItem *)getWhiteMenuBarButtonIcon
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menuIcon"] style:UIBarButtonItemStylePlain target:nil action:nil];
    buttonItem.tintColor = [UIColor whiteColor];
    return buttonItem;
}
+(UIBarButtonItem *)getPlusMenuBarButtonIcon
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"plusIcon"] style:UIBarButtonItemStylePlain target:nil action:nil];
    buttonItem.tintColor = [UIColor whiteColor];
    return buttonItem;
}

+(UIBarButtonItem *)getWhiteBackButtonIcon
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backButton"] style:UIBarButtonItemStylePlain target:nil action:nil];
    buttonItem.tintColor = [UIColor whiteColor];
    return buttonItem;
}

+(UIBarButtonItem *)getNotificationBarButtonIcon
{
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"bellIcon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:nil action:nil];
    return buttonItem;
}
#pragma mark -
#pragma mark Compress Image

+(NSData *)compressImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    if (actualHeight > maxHeight || actualWidth > maxWidth){ if(imgRatio < maxRatio)
    {
        //adjust width according to maxHeight
        imgRatio = maxHeight / actualHeight;
        actualWidth = imgRatio * actualWidth;
        actualHeight = maxHeight;
    }
    else
        if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth; }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size); [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return imageData;
}

@end
