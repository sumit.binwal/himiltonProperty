//
//  AppWebHandler.h
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//


#pragma mark -
#pragma mark Internet Check Bool
FOUNDATION_EXPORT BOOL isInternetAvailabel;

#pragma mark -
#pragma mark Background Session Identifier
static NSString *const BACKGROUNG_SESSION_IDENTIFIER = @"gilplug.background.download";


#pragma mark -
#pragma mark HttpMethodType Enum
typedef NS_ENUM(NSInteger,HttpMethodType)
{
    HttpMethodTypeGet = 0,// GET
    HttpMethodTypePost // POST
};

#pragma mark -
#pragma mark SuccessHandler Block
typedef void(^DataTaskSuccessfullHandler)(NSData *data, NSDictionary *dictionary, NSInteger statusCode);
#pragma mark -
#pragma mark ErrorHandler Block
typedef void(^DataTaskFailureHandler)(NSError *error);

#pragma mark -
#pragma mark -
#pragma mark CLASS INTERFACE
@interface AppWebHandler : NSObject

#pragma mark -
#pragma mark Properties
@property(nonatomic,strong)NSURLSession *httpSessionDefault;
@property(nonatomic,strong)NSURLSession *httpSessionBackground;
@property(nonatomic,strong)NSMutableArray *arrayDataTask;
@property(nonatomic,strong)NSURL *tempDirectory;
@property(nonatomic,strong)NSMutableDictionary *dictProgressTask;

#pragma mark -
#pragma mark Class Functions
+ (AppWebHandler *)sharedInstance;

#pragma mark -
#pragma mark Instance Functions

/**
 *  @author Vaibhav Khatri, 16-07-11 18:07:25
 *
 *  This function should be called when user wants to send only string format.
 This function creates NSURLSessionDataTask and passes the request url and other parameters and get response in callbacks methods, which user can check using blocks.
 *
 *  @param url            NSURL instance to hit
 *  @param httpMethodType HTTP request method type
 *  @param parameters     Parameters to be send along with request for processing. (ONLY FOR POST HTTP METHOD)
 *  @param deSerialize    BOOL to check, if user wants to deserialize the response data
 *  @param successHandler Custom Success block handler to help user
 *  @param failureHandler Custom Failure block handler to help user
 */
- (void)fetchDataFromUrl:(NSURL *)url httpMethod:(HttpMethodType)httpMethodType parameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler;



- (void)uploadDataFromURL:(NSURL *)url withUploadData:(NSData *)uploadData parameterName:(NSString *)paramaterName dataName:(NSString *)dataName mimeType:(NSString *)mimeType otherParameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler;


-(void)uploadMultipleImageDataFromURL:(NSURL *)url imageArray:(NSMutableArray *)imageArry otherParameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler;


-(void)cancelCurrentRequest;
/**
 *  @author Vaibhav Khatri, 16-07-11 18:07:13
 *
 *  Calling this function, initialises the background session object of NSURLSession.
 */
- (void)initializeBackgroundSession;

- (void)addDelegate:(id)delegate;

- (void)removeDelegate:(id)delegate;

- (void)removeDataLogout;

@end
