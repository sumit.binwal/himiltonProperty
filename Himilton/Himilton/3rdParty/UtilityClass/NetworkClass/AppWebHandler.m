//
//  AppWebHandler.m
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import "AppWebHandler.h"
#import "Reachability.h"


#pragma mark -
#pragma mark Internet Check Bool
BOOL isInternetAvailabel;

#pragma mark -
#pragma mark CLASS EXTENSION
@interface AppWebHandler ()<NSURLSessionDelegate>

#pragma mark -
@property(nonatomic)Reachability *internetRechability;
@property(nonatomic)NSHashTable *delegates;
//@property(nonatomic)dispatch_queue_t delegateQueue;

#pragma mark -
- (NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url;
- (NSMutableURLRequest *)createPostRequestWithUrl:(NSURL *)url andParmaters:(NSDictionary *)dictionary;
- (NSMutableURLRequest *)createMultiPartPostRequestWithUrl:(NSURL *)url data:(NSData *)data dataName:(NSString *)dataName paramName:(NSString *)paramName mimeType:(NSString *)mimeType andParameters:(NSDictionary *)parameters;

- (void)createTempDirectory;
- (void)removeDirectory;
- (void)initializeBackgroundSession;

@end
#pragma mark -

#pragma mark -
#pragma mark CLASS IMPLEMENTATION
@implementation AppWebHandler

#pragma mark -
#pragma mark Shared Handler
+(AppWebHandler *)sharedInstance
{
    static AppWebHandler *_webHandler;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _webHandler = [[self alloc]init];
    });
    return _webHandler;
}

#pragma mark -
#pragma mark Init
-(instancetype)init
{
    if (self = [super init])
    {
        // Default Session
        NSURLSessionConfiguration *_configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _httpSessionDefault = [NSURLSession sessionWithConfiguration:_configuration];
        
        [self initializeBackgroundSession];
        
        _arrayDataTask = [[NSMutableArray alloc]init];
        _dictProgressTask = [[NSMutableDictionary alloc]init];
        
        // Observer for network status
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
        
        self.internetRechability = [Reachability reachabilityForInternetConnection];
        [self.internetRechability startNotifier];
        [self checkNetworkStatus:nil];
        
        [self createTempDirectory];
        
        _delegates = [NSHashTable weakObjectsHashTable];
    }
    return self;
}

- (void)initializeBackgroundSession
{
    if (!_httpSessionBackground) {
        // Background Session
        NSURLSessionConfiguration *_configurationBackground = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:BACKGROUNG_SESSION_IDENTIFIER];
        _httpSessionBackground = [NSURLSession sessionWithConfiguration:_configurationBackground delegate:self delegateQueue:nil];
    }
}

- (void)addDelegate:(id)delegate
{
    if (![_delegates containsObject:delegate])
    {
        [_delegates addObject:delegate];
    }
}

- (void)removeDelegate:(id)delegate
{
    if ([_delegates containsObject:delegate])
    {
        [_delegates removeObject:delegate];
    }
}

- (void)removeDataLogout
{
    if (_delegates  && _delegates.count>0)
    {
        [self.delegates removeAllObjects];
    }
    if (_arrayDataTask && _arrayDataTask.count>0)
    {
        [self.arrayDataTask removeAllObjects];
    }
    if (_dictProgressTask && _dictProgressTask.count>0)
    {
        [self.dictProgressTask removeAllObjects];
    }
    if (_httpSessionBackground)
    {
        [self.httpSessionBackground invalidateAndCancel], self.httpSessionBackground = nil;
    }
    [self removeDirectory];
}

-(void)cancelCurrentRequest
{
    //nsurlsesstion
}


#pragma mark -
#pragma mark Data Fetch Method
- (void)fetchDataFromUrl:(NSURL *)url httpMethod:(HttpMethodType)httpMethodType parameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler
{
    // Created initial task with nil value
    NSURLSessionDataTask *dataTask = nil;
    // Created initial request with nil value
    NSMutableURLRequest *urlRequest = nil;
    NSLog(@"API URL : %@",url);
    // Case for Http Method used for data fetch
    switch (httpMethodType)
    {
        case HttpMethodTypeGet:// Get http method
        {
            // Calling method "createGetRequestWithUrl:" which will return new instance of 'NSMutableURLRequest' which will be used for Get functionality.
            urlRequest = [self createGetRequestWithUrl:url];
        }
            break;
            
        case HttpMethodTypePost:// Post http method
        {
            // Calling method "createPostRequestWithUrl: andParmaters:" which will return new instance of 'NSMutableURLRequest' which will be used for Post functionality.
            urlRequest = [self createPostRequestWithUrl:url andParmaters:parameters];
        }
            break;
            
        default:
            break;
    }
    
    // Using the single session created, to make and start a new task of type 'NSURLSessionDataTask'. Request created above is passed as a paramater to perform task, along with completion handler block which return data(if no error is found) and error(if some error encountered).
    dataTask = [_httpSessionDefault dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            NSLog(@"%@",error.localizedDescription);
            if (error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet ){
            }
            // Checking for error, if there is any
            if (error)
            {
                // If there is error, then checking the condition if its error handling block is nil.
                if (!failureHandler)
                {
                    // Nil, so returning from the method, user does not posted with anything.
                    return;
                }
                else
                {
                    // User is posted with the error.
                    failureHandler(error);
                    return;
                }
            }
            // If no completion handler, then return.
            if (!successHandler) {
                return;
            }
            
            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
            
            // Bool to check whether developer wants result in deserialized format ,i.e., in foundation object
            if (deSerialize)
            {
                // Converting data into NSDictionary object.
                NSError *deSerializeError;
                NSDictionary *deSerializedDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deSerializeError];
                
                // If error
                if (deSerializeError)
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if (!failureHandler)
                    {
                        return;
                    }
                    else
                    {
                        // User is posted with the error.
                        failureHandler(deSerializeError);
                        return;
                    }
                }
                else
                {
                    // Sending data as nil and dictionary object because 'deSerialize is TRUE'.
                    successHandler(nil,deSerializedDictionary,statusCode);
                }
            }
            else
            {
                // Sending data and dictionary as nil object because 'deSerialize is FALSE'.
                successHandler(data,nil,statusCode);
            }
        });
    }];
    
    // Start the data fetch process.
    [dataTask resume];
}

-(void)uploadDataFromURL:(NSURL *)url withUploadData:(NSData *)uploadData parameterName:(NSString *)paramaterName dataName:(NSString *)dataName mimeType:(NSString *)mimeType otherParameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler
{
    // Created initial task with nil value
    NSURLSessionDataTask *dataTask = nil;
    // Created initial request with nil value
    NSMutableURLRequest *urlRequest = nil;
    
    // Calling method "createPostRequestWithUrl: andParmaters:" which will return new instance of 'NSMutableURLRequest' which will be used for Post functionality.
    urlRequest = [self createMultiPartPostRequestWithUrl:url data:uploadData dataName:dataName paramName:paramaterName mimeType:mimeType andParameters:parameters];
    
    // Using the single session created, to make and start a new task of type 'NSURLSessionDataTask'. Request created above is passed as a paramater to perform task, along with completion handler block which return data(if no error is found) and error(if some error encountered).
    
    dataTask = [_httpSessionDefault dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            
            if (error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet ){
            }
            // Checking for error, if there is any
            if (error)
            {
                // If there is error, then checking the condition if its error handling block is nil.
                if (!failureHandler)
                {
                    // Nil, so returning from the method, user does not posted with anything.
                    return;
                }
                else
                {
                    // User is posted with the error.
                    failureHandler(error);
                    return;
                }
            }
            // If no completion handler, then return.
            if (!successHandler) {
                return;
            }
            
            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
            
            // Bool to check whether developer wants result in deserialized format ,i.e., in foundation object
            if (deSerialize)
            {
                // Converting data into NSDictionary object.
                NSError *deSerializeError;
                NSDictionary *deSerializedDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deSerializeError];
                
                // If error
                if (deSerializeError)
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if (!failureHandler)
                    {
                        return;
                    }
                    else
                    {
                        // User is posted with the error.
                        failureHandler(deSerializeError);
                        return;
                    }
                }
                else
                {
                    // Sending data as nil and dictionary object because 'deSerialize is TRUE'.
                    successHandler(nil,deSerializedDictionary,statusCode);
                }
            }
            else
            {
                // Sending data and dictionary as nil object because 'deSerialize is FALSE'.
                successHandler(data,nil,statusCode);
            }
        });
    }];
    [dataTask resume];
}

-(void)uploadMultipleImageDataFromURL:(NSURL *)url imageArray:(NSMutableArray *)imageArry otherParameters:(NSDictionary *)parameters shouldDeSerialize:(BOOL)deSerialize success:(DataTaskSuccessfullHandler)successHandler failure:(DataTaskFailureHandler)failureHandler
{
    // Created initial task with nil value
    NSURLSessionDataTask *dataTask = nil;
    // Created initial request with nil value
    NSMutableURLRequest *urlRequest = nil;
    
    // Calling method "createPostRequestWithUrl: andParmaters:" which will return new instance of 'NSMutableURLRequest' which will be used for Post functionality.
    
    urlRequest = [self createMultiImageMultiPartPostRequestWithUrl:url ImageData:imageArry andParameters:parameters];
    
    // Using the single session created, to make and start a new task of type 'NSURLSessionDataTask'. Request created above is passed as a paramater to perform task, along with completion handler block which return data(if no error is found) and error(if some error encountered).
    
    dataTask = [_httpSessionDefault dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            
            if (error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet ){
            }
            // Checking for error, if there is any
            if (error)
            {
                // If there is error, then checking the condition if its error handling block is nil.
                if (!failureHandler)
                {
                    // Nil, so returning from the method, user does not posted with anything.
                    return;
                }
                else
                {
                    // User is posted with the error.
                    failureHandler(error);
                    return;
                }
            }
            // If no completion handler, then return.
            if (!successHandler) {
                return;
            }
            
            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
            
            // Bool to check whether developer wants result in deserialized format ,i.e., in foundation object
            if (deSerialize)
            {
                // Converting data into NSDictionary object.
                NSError *deSerializeError;
                NSDictionary *deSerializedDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&deSerializeError];
                
                // If error
                if (deSerializeError)
                {
                    // If there is error, then checking the condition if its error handling block is nil.
                    if (!failureHandler)
                    {
                        return;
                    }
                    else
                    {
                        // User is posted with the error.
                        failureHandler(deSerializeError);
                        return;
                    }
                }
                else
                {
                    // Sending data as nil and dictionary object because 'deSerialize is TRUE'.
                    successHandler(nil,deSerializedDictionary,statusCode);
                }
            }
            else
            {
                // Sending data and dictionary as nil object because 'deSerialize is FALSE'.
                successHandler(data,nil,statusCode);
            }
        });
    }];
    [dataTask resume];
}



#pragma mark -
#pragma mark Get Request Generation Method
- (NSMutableURLRequest *)createGetRequestWithUrl:(NSURL *)url
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:120];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setHTTPMethod:@"GET"];
    
    return urlRequest;
}


#pragma mark -
#pragma mark Post Request Generation Method
- (NSMutableURLRequest *)createPostRequestWithUrl:(NSURL *)url andParmaters:(NSDictionary *)dictionary
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:HEADER_VERSION forHTTPHeaderField:@"Accept-Version"];
    [urlRequest addValue:HEADER_LANG forHTTPHeaderField:@"Accept-Language"];
    [urlRequest addValue:HEADER_DEVICE forHTTPHeaderField:@"Device-Os"];
    NSLog(@"user TOKEN VALUE : %@",[UtilityClass getUserInfoLoginTokenValue]);
//    if ([UtilityClass getUserInfoLoginTokenValue]) {
//        [urlRequest addValue:[UtilityClass getUserInfoLoginTokenValue] forHTTPHeaderField:@"Authorization"];
//    }
    
    [urlRequest setHTTPMethod:@"POST"];
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
    NSString *str = [[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding];
    str = [str stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    postData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",[[NSString alloc]initWithData:postData encoding:NSUTF8StringEncoding]);
    if (error)
    {
        NSString *postBody = [NSString string];
        
        for (NSString *key in dictionary.allKeys)
        {
            id anyObject = dictionary[key];
            postBody = [postBody stringByAppendingString:[NSString stringWithFormat:@"%@=%@ ",key,anyObject]];
        }
        postBody = [postBody stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        [urlRequest setHTTPBody:[postBody dataUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        [urlRequest setHTTPBody:postData];
    }
    return urlRequest;
}

- (NSMutableURLRequest *)createMultiPartPostRequestWithUrl:(NSURL *)url data:(NSData *)data dataName:(NSString *)dataName paramName:(NSString *)paramName mimeType:(NSString *)mimeType andParameters:(NSDictionary *)parameters
{
    NSString *boundary = [NSString stringWithFormat:@"Boundary+%08X%08X", arc4random(), arc4random()];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:HEADER_VERSION forHTTPHeaderField:@"Accept-Version"];
    [urlRequest addValue:HEADER_LANG forHTTPHeaderField:@"Accept-Language"];
    [urlRequest addValue:HEADER_DEVICE forHTTPHeaderField:@"Device-Os"];
    if ([UtilityClass getUserInfoLoginTokenValue]) {
    [urlRequest addValue:[UtilityClass getUserInfoLoginTokenValue] forHTTPHeaderField:@"Authorization"];
    }
    [urlRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    
    if (data.length>0) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpeg\"\r\n", paramName, dataName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:data];
    }
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    for (NSInteger i=0; i<parameters.count; i++)
    {
        NSString *param = parameters.allKeys[i];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", param, parameters[param]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // setting the body of the post to the reqeust
    [urlRequest setHTTPBody:body];
    
//    // set the content-length
//    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
//    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    return urlRequest;
}

- (NSMutableURLRequest *)createMultiImageMultiPartPostRequestWithUrl:(NSURL *)url ImageData:(NSMutableArray *)imageArr andParameters:(NSDictionary *)parameters
{
    NSString *boundary = [NSString stringWithFormat:@"Boundary+%08X%08X", arc4random(), arc4random()];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:HEADER_VERSION forHTTPHeaderField:@"Accept-Version"];
    [urlRequest addValue:HEADER_LANG forHTTPHeaderField:@"Accept-Language"];
    [urlRequest addValue:HEADER_DEVICE forHTTPHeaderField:@"Device-Os"];
    if ([UtilityClass getUserInfoLoginTokenValue]) {
        [urlRequest addValue:[UtilityClass getUserInfoLoginTokenValue] forHTTPHeaderField:@"Authorization"];
    }
    [urlRequest setHTTPMethod:@"POST"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    for (NSInteger i=0; i<imageArr.count; i++) {
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpeg\"\r\n", imageArr[i][@"imageName"], imageArr[i][@"imageName"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageArr[i][@"imageData"]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    
    for (NSInteger i=0; i<parameters.count; i++)
    {
        NSString *param = parameters.allKeys[i];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", param, parameters[param]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // setting the body of the post to the reqeust
    [urlRequest setHTTPBody:body];
    
    //    // set the content-length
    //    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    //    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    return urlRequest;
}

#pragma mark -
#pragma mark Internet Handling
-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [self.internetRechability currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            isInternetAvailabel = NO;
            
            /**
             *  received on home play view controller (fast mode), used for showing 'connectionLabel'
             */
            [[NSNotificationCenter defaultCenter] postNotificationName:@"internetDisonnected" object:self];
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            isInternetAvailabel = YES;
            
            /**
             *  received on home play view controller (fast mode), used for continue loading records
             */
            [[NSNotificationCenter defaultCenter] postNotificationName:@"internetConnected" object:self];
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            isInternetAvailabel = YES;
            
            /**
             *  received on home play view controller (fast mode), used for continue loading records
             */
            [[NSNotificationCenter defaultCenter] postNotificationName:@"internetConnected" object:self];
            
            break;
        }
    }
}

- (void)createTempDirectory
{
    NSURL *directoryURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"Feeds"] isDirectory:YES];
    [[NSFileManager defaultManager] createDirectoryAtURL:directoryURL withIntermediateDirectories:YES attributes:nil error:nil];
    _tempDirectory = directoryURL;
}

- (void)removeDirectory
{
    NSURL *directoryURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"Feeds"] isDirectory:YES];
    
//    if ([[NSFileManager defaultManager]fileExistsAtPath:directoryURL.absoluteString isDirectory:nil])
//    {
        [[NSFileManager defaultManager]removeItemAtURL:directoryURL error:NULL];
//    }
    
    [self createTempDirectory];
}

@end
