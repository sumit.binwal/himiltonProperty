//
//  UIStoryboard+References.m
//  CarryTruckApp
//
//  Created by Santosh on 18/01/17.
//  Copyright © 2017 Kipl. All rights reserved.
//

#import "UIStoryboard+References.h"

@implementation UIStoryboard (References)



+ (UIStoryboard *)getReceiptStoryboard
{
    return [UIStoryboard storyboardWithName:@"Receipt" bundle:[NSBundle mainBundle]];
}
+ (UIStoryboard *)getHomeBookingStoryboard
{
    return [UIStoryboard storyboardWithName:@"Home" bundle:[NSBundle mainBundle]];
}

+ (UIStoryboard *)getSideMenuStoryboard
{
    return [UIStoryboard storyboardWithName:@"SideMenu" bundle:[NSBundle mainBundle]];
}
+ (UIStoryboard *)getLoginFlowStoryboard
{
    return [UIStoryboard storyboardWithName:@"LoginFlow" bundle:[NSBundle mainBundle]];
}

+ (UIStoryboard *)getMyAccountStoryboard
{
    return [UIStoryboard storyboardWithName:@"MyAccount" bundle:[NSBundle mainBundle]];
}

+ (UIStoryboard *)getProfileStoryboard
{
    return [UIStoryboard storyboardWithName:@"MyProfile" bundle:[NSBundle mainBundle]];
}

@end
