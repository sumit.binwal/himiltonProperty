//
//  UIStoryboard+References.h
//  CarryTruckApp
//
//  Created by Santosh on 18/01/17.
//  Copyright © 2017 Kipl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (References)

+ (UIStoryboard *)getReceiptStoryboard;
+ (UIStoryboard *)getHomeBookingStoryboard;
+ (UIStoryboard *)getSideMenuStoryboard;
+ (UIStoryboard *)getLoginFlowStoryboard;

+ (UIStoryboard *)getMyAccountStoryboard;

+ (UIStoryboard *)getProfileStoryboard;


@end
