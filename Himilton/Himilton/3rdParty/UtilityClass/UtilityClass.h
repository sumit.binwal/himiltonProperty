//
//  UtilityClass.h
//  Tabco App
//
//  Created by Santosh on 17/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//


#pragma mark -
#pragma mark CLASS INTERFACE
@interface UtilityClass : NSObject
#pragma mark -
+ (NSString *)getUserInfoLoginTokenValue;
#pragma mark -
+ (void)showHud;
#pragma mark -
+ (void)showNetworkIndicator;
#pragma mark -
+ (void)hideNetworkIndicator;
#pragma mark -
+ (void)hideHud;
#pragma mark -
+ (UIImage *)convertBase64StringToImage:(NSString *)base64String;
#pragma mark -
+ (NSString *)convertImageToBase64String:(UIImage *)image;
#pragma mark -
+ (NSData *)convertDictionaryToData:(NSDictionary *)dictionary;
#pragma mark -
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock;
#pragma mark -
+ (void)showSheetWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock;
#pragma mark -
+ (void)showEmailFieldAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex,NSArray *fieldsArray))dismissBlock;
#pragma mark -
+ (void)saveUserInfoWithDictionary:(NSDictionary *)dictionary;
#pragma mark -
+ (NSDictionary *)getUserInfoDictionary;

#pragma mark -
+ (void)deleteUserInfoDictionary;

#pragma mark -
+ (NSString *)getUserInfoImagesBasePath;

#pragma mark -
+ (NSString *)getUserInfoFirstNameValue;
#pragma mark -
+ (NSString *)getUserInfoLastNameValue;

#pragma mark -
+ (NSString *)getUserInfoEmailValue;

#pragma mark -
+(NSMutableArray *)convertImageBytesToArrayUsingImage:(UIImage *)image;
#pragma mark -
+(BOOL)isValidEmail:(NSString *)checkString;
#pragma mark -
+ (BOOL)isValueNotEmpty:(NSString*)aString;
#pragma mark -
+ (BOOL)isValidMobileNumber:(NSString*)number;
#pragma mark -
+(NSString *)trimSpaceInString:(NSString *)mainstr;
#pragma mark -
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize;
#pragma mark -
+(UIImage *)cropImage:(UIImage *)image toResolution:(CGFloat)resolution;
#pragma mark -
+ (UIView *)SnapShot:(UIView *)theView;
#pragma mark -
+(BOOL)isUpdateOrLoginRequired:(NSDictionary *)dictionary fromViewController:(UIViewController *)viewVontroller;
#pragma mark -
+(void)addGlobalBackgroundOn:(UIView *)view;
#pragma mark -
+(void)removeGlobalBackgroundFrom:(UIView *)view;
#pragma mark -
+(void)clearDefaultsAndCache;


+ (void)saveVehicleInfoWithDictionary:(NSDictionary *)dictionary;

#pragma mark -
#pragma mark Get User Info
+ (NSDictionary *)getVehicleInfoDictionary;

#pragma mark -
#pragma mark Set Navigation Bar
+ (void)setNavigationBar:(UINavigationController *)navController;

#pragma mark -
+(CGFloat)heightOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize;
#pragma mark -
+ (NSString *)getFormattedStringUsingDateString:(NSString *)dateString;
#pragma mark -
+(NSString *)convertTimeStampToString:(double)timeStamp;
#pragma mark -
+(NSString *) convertTimeStampToDate :(NSDate *) date;

#pragma mark -
+(void)showNetworkIndicator;
#pragma mark -
+(void)hideNetworkIndicator;

#pragma mark-
+(BOOL)checkNullString:(NSString *)str;
#pragma mark -
+(NSString*)date2str:(NSDate*)myNSDateInstance withFormat:(NSString *)strFormat;

#pragma mark -
#pragma mark - Draw Image View In Circle With Image Border Color And Width
+ (void)drawImageViewCircle:(UIImageView *)imgVw imageBorderWidth:(float)borderWidth borderColor:(UIColor *)borderColor;

#pragma mark -
#pragma mark - Add Top Border To A View
+(void)addTopBorder:(UIView *)view rgb:(UIColor*)rgb;

#pragma mark -
#pragma mark - Add Bottom Border To A View
+(void)addBottomBorder:(UIView *)view rgb:(UIColor*)rgb;

#pragma mark - Methods related to chat
+(NSString *)convertEmojiToUnicode:(NSString *)str_sendMessage;
+(NSString *)convertUnicodeToEmoji:(NSString *)str_receiveMessage;
+(NSString *)convertDictionaryToJsonString:(NSDictionary *)dict;
+(NSDictionary *)convertJsonStrToDictionary:(NSString *)jsonStr;


+(UIViewController *)existsViewController:(Class)viewControllerClass inside:(UINavigationController *)navigationController;

+(UIBarButtonItem *)getPlusMenuBarButtonIcon;
+(UIBarButtonItem *)getMenuBarButtonIcon;
+(UIBarButtonItem *)getWhiteMenuBarButtonIcon;
+(UIBarButtonItem *)getWhiteBackButtonIcon;
+(UIBarButtonItem *)getNotificationBarButtonIcon;


#pragma mark -
#pragma mark Compress Image

+(NSData *)compressImage:(UIImage *)image;

@end
