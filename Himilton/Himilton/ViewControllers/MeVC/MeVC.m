//
//  MeVC.m
//  Himilton
//
//  Created by Sumit Sharma on 5/29/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "MeVC.h"
#import "TabIndexCollectionCell.h"
#import "TableContentCell.h"
#import "LoginVC.h"
#import <SafariServices/SafariServices.h>
typedef enum : NSUInteger {
    TabAccountSetting,
    TabBuildingInfo,
    TabLandInfo,
    TabBuildingUpdate,
    TabCustomerCare,
    TabFeedback,
} TopTabType;

@interface MeVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,SFSafariViewControllerDelegate>
{
    IBOutlet UICollectionView *collectionVwTabTitle;
    IBOutlet UITableView *tabelVwContent;
    NSMutableArray *arrTopTabContaint;
    NSMutableArray *arrTableViewContent;
    NSInteger selectedTopTab;
}
@end

@implementation MeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    
    
    // Do any additional setup after loading the view.
}
-(void)setupView
{
    arrTopTabContaint =[[NSMutableArray alloc]initWithObjects:@"Account Settings",@"Building Information",@"Land Information",@"Building Updates",@"Customer Care",@"Feedback", nil];
    
     if ([notificationFlag integerValue]>0)
     {
         selectedTopTab=[notificationFlag integerValue];
         arrTableViewContent=[[NSMutableArray alloc]init];
         
         
         switch (selectedTopTab) {
             case 0:
             {
                 
             }
             case 1:
             {
                 arrTableViewContent=[UtilityClass getUserInfoDictionary][@"BuildingInformationDetails"];
                 break;
             }
             case 2:
             {
                 arrTableViewContent=[UtilityClass getUserInfoDictionary][@"landInformationDetails"];
                 break;
             }
             case 3:
             {
                 arrTableViewContent=[UtilityClass getUserInfoDictionary][@"buildingUpdates"];
                 
                 break;
             }
                 
             default:
                 break;
         }
         [tabelVwContent reloadData];
     }
    else
    {
        selectedTopTab=0;
    }
    
    
    UICollectionViewFlowLayout *layout=(UICollectionViewFlowLayout *) collectionVwTabTitle.collectionViewLayout;
    layout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [tabelVwContent reloadData];
    [collectionVwTabTitle reloadData];
    
    if ([notificationFlag integerValue]>0)
    {
        selectedTopTab=[notificationFlag integerValue];
        arrTableViewContent=[[NSMutableArray alloc]init];
        
        
        switch (selectedTopTab) {
            case 0:
            {
                
            }
            case 1:
            {
                arrTableViewContent=[UtilityClass getUserInfoDictionary][@"BuildingInformationDetails"];
                break;
            }
            case 2:
            {
                arrTableViewContent=[UtilityClass getUserInfoDictionary][@"landInformationDetails"];
                break;
            }
            case 3:
            {
                arrTableViewContent=[UtilityClass getUserInfoDictionary][@"buildingUpdates"];
                break;
            }
            default:
                break;
        }
        [tabelVwContent reloadData];
    }
    else
    {
        selectedTopTab=0;
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    notificationFlag=0;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView Delegate method

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrTopTabContaint.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
        
//    return CGSizeMake(cell.lblTabTitle.frame.size.width, cell.lblTabTitle.frame.size.height);
    return CGSizeMake(20, 40);
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"TabIndexCollectionCell";
    
    TabIndexCollectionCell *cell = (TabIndexCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.lblTabTitle.text=[NSString stringWithFormat:@"%@",[arrTopTabContaint[indexPath.row] uppercaseString]];
    cell.vwBottom.backgroundColor=[UIColor clearColor];

    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (![UtilityClass getUserInfoDictionary]) {
        
        LoginVC *lvc=[self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:lvc animated:YES completion:nil];
        return;
    }

    selectedTopTab=(long)indexPath.row;
    arrTableViewContent=[[NSMutableArray alloc]init];
    
    switch (indexPath.row) {
        case 0:
        {
            
        }
        case 1:
        {
            arrTableViewContent=[UtilityClass getUserInfoDictionary][@"BuildingInformationDetails"];
            break;
        }
        case 2:
        {
            arrTableViewContent=[UtilityClass getUserInfoDictionary][@"landInformationDetails"];
            break;
        }
        case 3:
        {
            arrTableViewContent=[UtilityClass getUserInfoDictionary][@"buildingUpdates"];

            break;
        }

        default:
            break;
    }
    [tabelVwContent reloadData];
}

#pragma mark - TableView Delegate method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(selectedTopTab==0 ||selectedTopTab==4 ) {
        return 1 * scaleFactorX;
    }

    else
    {
        return arrTableViewContent.count;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedTopTab==0) {
        return 155 * scaleFactorX;
    }
else if (selectedTopTab==4)
{
    return 260*scaleFactorX;
}
else
    {
        return 30 * scaleFactorX;
    }
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = nil;
    
    if (selectedTopTab==0)
    {
        cellIdentifier = @"accountSettingCell";

    }
    else if (selectedTopTab==4)
    {
        cellIdentifier=@"customerCareCell";
    }
    else
    {
        cellIdentifier = @"TableContentCell";
    }
    
    TableContentCell *cell = (TableContentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if (selectedTopTab==0) {
        
        if (![UtilityClass getUserInfoDictionary])
        {
            cell.labelEmailAddress.text=@"Guest User";
            [cell.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        }
        else
        {
            cell.labelEmailAddress.text=[NSString stringWithFormat:@"%@",[UtilityClass getUserInfoDictionary][@"user_email"]];
            [cell.btnLogin setTitle:@"Logout" forState:UIControlStateNormal];
        }
        [cell.btnLogin addTarget:self action:@selector(loginButtonCliced:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (selectedTopTab==4)
    {
        cell.labelCCSupervisorName.text=[NSString stringWithFormat:@"%@",[UtilityClass getUserInfoDictionary][@"supervisor_name"]];
        cell.labelCCSupervisorEmail.text=[NSString stringWithFormat:@"%@",[UtilityClass getUserInfoDictionary][@"supervisor_email"]];
        cell.labelCCSupervisorPhone.text=[NSString stringWithFormat:@"%@",[UtilityClass getUserInfoDictionary][@"supervisor_phone"]];
    }
    else
    {
        cell.lblTitle.text=[NSString stringWithFormat:@"%@",arrTableViewContent[indexPath.row][@"title"]];

    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (selectedTopTab!=0 && arrTableViewContent.count>0) {
        NSString *strUrl=[NSString stringWithFormat:@"%@",arrTableViewContent[indexPath.row][@"url"]];
        NSURL *url=[NSURL URLWithString:strUrl];
        SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:url];
        svc.delegate = self;
        [self presentViewController:svc animated:YES completion:nil];
    }
}

#pragma mark - Safari View Controller delegate methods
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller
{
    [self dismissViewControllerAnimated:true completion:nil];
}


-(IBAction)loginButtonCliced:(id)sender
{
    if (![UtilityClass getUserInfoDictionary])
    {
        LoginVC *lvc=[self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:lvc animated:YES completion:nil];
    }
    else
    {
        [UtilityClass deleteUserInfoDictionary];
    }
}


@end
