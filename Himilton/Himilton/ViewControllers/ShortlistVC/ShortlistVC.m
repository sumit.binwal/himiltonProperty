//
//  ShortlistVC.m
//  Himilton
//
//  Created by Sumit Sharma on 5/29/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "ShortlistVC.h"
#import "LoginVC.h"
#import "PropertieListCell.h"
#import "OfferViewController.h"

#import "PropertyDetailViewController.h"
@interface ShortlistVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
{
    NSMutableArray *arrPropertie;
    IBOutlet UITableView *tblVwShortlisted;
    IBOutlet UILabel *lblMsg;
    IBOutlet UIButton *btnLogin;
}
@end

@implementation ShortlistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrPropertie=[NSMutableArray new];
    self.view.backgroundColor=[UIColor redColor];
        // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    if (![UtilityClass getUserInfoDictionary]) {
        
        lblMsg.text=@"To use this feature,\nplease login first";
        [btnLogin setHidden:NO];
        
    }
    else
    {
        [self fetchPropertieListingFromServerAPI];
    }
    
   

}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Delegate method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrPropertie.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 264 * scaleFactorX;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"proptieList";
    
    PropertieListCell *cell = (PropertieListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.labelBed.text=[NSString stringWithFormat:@"%@",arrPropertie[indexPath.row][@"No_of_Badrooms"]];
    cell.labelCar.text=[NSString stringWithFormat:@"%@",arrPropertie[indexPath.row][@"No_of_cars"]];
    cell.labelBath.text=[NSString stringWithFormat:@"%@",arrPropertie[indexPath.row][@"No_of_Bathrooms"]];
    cell.labelAddress.text=[NSString stringWithFormat:@"%@",arrPropertie[indexPath.row][@"Address"][@"address"]];
    NSString *displayStr = [NSNumberFormatter localizedStringFromNumber:@([arrPropertie[indexPath.row][@"price"] integerValue])
                                                         numberStyle:NSNumberFormatterCurrencyAccountingStyle];
    displayStr = [displayStr stringByReplacingOccurrencesOfString:@".00" withString:@""];
    
    cell.labelPrice.text=[NSString stringWithFormat:@"%@",displayStr];
    [cell.btnOurOffer addTarget:self action:@selector(ourOfferBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    id propertyImage = nil;
    if ([arrPropertie[indexPath.row][@"PropertyImage"] isKindOfClass:[NSString class]]) {
        propertyImage = arrPropertie[indexPath.row][@"PropertyImage"];
    } else if ([arrPropertie[indexPath.row][@"PropertyImage"] isKindOfClass:[NSArray class]]) {
        NSArray *images = arrPropertie[indexPath.row][@"PropertyImage"];
        propertyImage = images[0];
        
        NSString *strUrl=[NSString stringWithFormat:@"%@",images[0]];
        if (strUrl.length<7) {
            propertyImage=nil;
        }
    }
    if (propertyImage != nil) {
        [cell.imgVwPropertie setImageWithURL:[NSURL URLWithString:propertyImage] placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    cell.buttonImageLike.tag=indexPath.row;
    
    [cell.buttonImageLike addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    if ([arrPropertie[indexPath.row][@"islike"]boolValue]) {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
    }
    else
    {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
    }

    return cell;
}

-(IBAction)likeButtonClicked:(UIButton *)sender
{
    if (![UtilityClass getUserInfoDictionary])
    {
        [UtilityClass showAlertWithTitle:APP_NAME message:@"To use this feature, plesae login first" onViewController:self withButtonsArray:@[@"Cancel",@"OK"] dismissBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex==1) {
                LoginVC *lvc=[self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:lvc animated:YES completion:nil];            }
        }];
        return;
    }
    
    
    PropertieListCell *cell=(PropertieListCell *)[tblVwShortlisted cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    NSURL *apiURL;
    
    if ([arrPropertie[sender.tag][@"islike"]boolValue]) {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
        
        apiURL=[NSURL URLWithString:API_UNLIKE];
        
        NSMutableDictionary *tempDict=[arrPropertie[sender.tag]mutableCopy];
        [tempDict setObject:@"0" forKey:@"islike"];
        [arrPropertie replaceObjectAtIndex:sender.tag withObject:tempDict];
    }
    else
    {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
        apiURL=[NSURL URLWithString:API_LIKE];
        
        NSMutableDictionary *tempDict=[arrPropertie[sender.tag]mutableCopy];
        [tempDict setObject:@"1" forKey:@"islike"];
        [arrPropertie replaceObjectAtIndex:sender.tag withObject:tempDict];
    }
    
    
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    
    [parameter setObject:SERVICE_KEY forKey:@"key"];
    [parameter setObject:[UtilityClass getUserInfoLoginTokenValue] forKey:@"userId"];
    [parameter setObject:arrPropertie[sender.tag][@"PropertyId"] forKey:@"propertyId"];
    __weak typeof(self)weakSelf = self;
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        [UtilityClass hideHud];
        if (statusCode==200) {
            
            
            
        }
        else
        {
            if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            else
            {
                [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            
            if ([arrPropertie[sender.tag][@"islike"]boolValue]) {
                [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
                
                NSMutableDictionary *tempDict=[arrPropertie[sender.tag]mutableCopy];
                [tempDict setObject:@"0" forKey:@"islike"];
                [arrPropertie replaceObjectAtIndex:sender.tag withObject:tempDict];
                
            }
            else
            {
                [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
                
                NSMutableDictionary *tempDict=[arrPropertie[sender.tag]mutableCopy];
                [tempDict setObject:@"1" forKey:@"islike"];
                [arrPropertie replaceObjectAtIndex:sender.tag withObject:tempDict];
            }
        }
    } failure:^(NSError *error) {
        [UtilityClass hideHud];
        [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
        
        if ([arrPropertie[sender.tag][@"islike"]boolValue]) {
            [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
            
            NSMutableDictionary *tempDict=[arrPropertie[sender.tag]mutableCopy];
            [tempDict setObject:@"0" forKey:@"islike"];
            [arrPropertie replaceObjectAtIndex:sender.tag withObject:tempDict];
            
        }
        else
        {
            [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
            
            NSMutableDictionary *tempDict=[arrPropertie[sender.tag]mutableCopy];
            [tempDict setObject:@"1" forKey:@"islike"];
            [arrPropertie replaceObjectAtIndex:sender.tag withObject:tempDict];
        }    }];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
    PropertyDetailViewController *pdvc=[self.storyboard instantiateViewControllerWithIdentifier:@"propertyDetailVC"];
    pdvc.hidesBottomBarWhenPushed=YES;
    pdvc.dictData=arrPropertie[indexPath.row];
    [self.navigationController pushViewController:pdvc animated:YES];
}

- (IBAction)ourOfferBtnAction:(id)sender
{
    OfferViewController *offerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"offerVC"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:offerVC];
    
    [self presentViewController:navController animated:YES completion:nil];
}


#pragma mark -
#pragma mark : TableView Cell Custome Method - Get Propertie Listing Action
-(void)fetchPropertieListingFromServerAPI
{
    
    [UtilityClass showHud];
    NSURL *apiURL=[NSURL URLWithString:API_SHORTLIST_LIST];
    NSDictionary *parameter=@{
                              @"key":SERVICE_KEY,
                              @"userId":[UtilityClass getUserInfoLoginTokenValue],
                              };
    
    __weak typeof(self)weakSelf = self;
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        [UtilityClass hideHud];
        if (statusCode==200) {
            arrPropertie=[dictionary [@"data"]mutableCopy];
            
            if (arrPropertie.count>0) {
                
                
                lblMsg.text=@"";
                [btnLogin setHidden:YES];
                
            }
            else{
                lblMsg.text=@"No Shortlisted Property\n Found";
                [btnLogin setHidden:YES];
            }
            
            [tblVwShortlisted reloadData];

            
        }
        else
        {
            if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            else
            {
                [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
        }
    } failure:^(NSError *error) {
        [UtilityClass hideHud];
        [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
    }];
    
}
- (IBAction)loginBtnClicked:(id)sender {
    LoginVC *lvc=[self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:lvc animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
