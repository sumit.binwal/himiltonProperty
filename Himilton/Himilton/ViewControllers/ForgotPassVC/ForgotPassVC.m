//
//  ForgotPassVC.m
//  Himilton
//
//  Created by Sumit Sharma on 6/4/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "ForgotPassVC.h"

@interface ForgotPassVC ()
{
    
    IBOutlet UITextField *txtFldEmail;
}
@end

@implementation ForgotPassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [txtFldEmail setValue:[UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnLOginButtonTap:(id)sender
{
    [self.view endEditing:YES];
    if (![UtilityClass isValueNotEmpty:txtFldEmail.text]) {
        [KiplToastView showToastOver:self.view message:@"Please Enter Email" duration:1.5];
    }
    else if (![UtilityClass isValidEmail:txtFldEmail.text]) {
        [KiplToastView showToastOver:self.view message:@"Please Enter Correct Email" duration:1.5];
    }
    
    else
    {
    // [UtilityClass showAlertWithTitle:refreshedToken message:refreshedToken onViewController:self withButtonsArray:nil dismissBlock:nil];
        
        [UtilityClass showHud];
        NSURL *apiURL=[NSURL URLWithString:API_FORGOT_PASSWORD];
        NSDictionary *parameter=@{
                                  @"key":SERVICE_KEY,
                                  @"txtFldEmail":txtFldEmail.text,
                                  
                                  };
        
        __weak typeof(self)weakSelf = self;
        
        [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
            
            [UtilityClass hideHud];
            if (statusCode==200) {
                
                if ([dictionary[@"status"]boolValue]) {
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"msg"] onViewController:self withButtonsArray:@[@"ok"] dismissBlock:^(NSInteger buttonIndex) {
                        
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
                else
                {
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"msg"] onViewController:self withButtonsArray:@[@"ok"] dismissBlock:^(NSInteger buttonIndex) {
                        
                        // [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
            }
            else
            {
                if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
                }
                else
                {
                    [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
                }
            }
        } failure:^(NSError *error) {
            [UtilityClass hideHud];
            [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
        }];
    }
}

@end
