//
//  OfferViewController.m
//  Himilton
//
//  Created by Sumit Sharma on 6/22/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "OfferViewController.h"
#import "OfferCustomeCell.h"
#import "OfferDetailViewController.h"
@interface OfferViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrOfferList;
    IBOutlet UITableView *tableViewOffers;
}
@end

@implementation OfferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNavigationBar];
    arrOfferList=[NSMutableArray new];
    // Do any additional setup after loading the view.
}
-(void)setUpNavigationBar
{
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationItem setTitle:@"Offers"];
    UIBarButtonItem *backBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:backBtn];
}

-(void)backButtonClicked
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self fetchOfferListFromServerAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOfferList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 185*scaleFactorX;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"OfferCustomeCell";
    
    OfferCustomeCell *cell = (OfferCustomeCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    NSString *imgURl=arrOfferList [indexPath.row][@"OfferImage"][0];
    [cell.imageViewOffer setImageWithURL:[NSURL URLWithString:imgURl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.labelOfferTitle.text=arrOfferList [indexPath.row][@"OfferName"];
        return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
    OfferDetailViewController *offerDetailVC=[self.storyboard instantiateViewControllerWithIdentifier:@"offerDetailVC"];
    
    offerDetailVC.dictOfferData=arrOfferList[indexPath.row];
    [self.navigationController pushViewController:offerDetailVC animated:YES];
}


#pragma mark -
#pragma mark : TableView Cell Custome Method - Get Propertie Listing Action
-(void)fetchOfferListFromServerAPI
{
    
    [UtilityClass showHud];
    NSURL *apiURL;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
        apiURL=[NSURL URLWithString:API_GET_OFFER_LIST];
    
        [parameter setObject:SERVICE_KEY forKey:@"key"];
  
    
    __weak typeof(self)weakSelf = self;
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        [UtilityClass hideHud];
        if (statusCode==200) {
            
            arrOfferList=[dictionary[@"data"]mutableCopy];
            [tableViewOffers reloadData];
        }
        else
        {
            if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            else
            {
                [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
        }
    } failure:^(NSError *error) {
        [UtilityClass hideHud];
        [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
