//
//  OfferDetailViewController.m
//  Himilton
//
//  Created by Sumit Sharma on 6/22/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "OfferDetailViewController.h"
#import "OfferDetailCustomeCell.h"
#import "AppointmentViewController.h"
#import <SafariServices/SafariServices.h>

#import <MessageUI/MessageUI.h>
@interface OfferDetailViewController ()<SFSafariViewControllerDelegate,MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UITableView *tableViewOfferDetail;
    IBOutlet UIView *vwTableViewFotter;
}
- (IBAction)offerLinkClicked:(id)sender;

@end

@implementation OfferDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    tableViewOfferDetail.tableFooterView=vwTableViewFotter;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    tableViewOfferDetail.estimatedRowHeight=20.0f;
    tableViewOfferDetail.rowHeight=UITableViewAutomaticDimension;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView Delegate Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"OfferDetailCustomeCell";
    
    OfferDetailCustomeCell *cell = (OfferDetailCustomeCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    

    cell.labelOfferTitle.text=self.dictOfferData[@"OfferName"];
    cell.labelOfferDetail.text=self.dictOfferData[@"OfferDescription"];
    return cell;
}

#pragma mark - IBActions

- (IBAction)btnCallTapped:(id)sender {
    
    /*    if (![UtilityClass getUserInfoDictionary]) {
     
     [UtilityClass showAlertWithTitle:APP_NAME message:@"To use this feature, plesae login first" onViewController:self withButtonsArray:@[@"ok"] dismissBlock:nil];
     return;
     }*/
    
    NSString *phoneNumber = adminPhoneNumber;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phoneNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)btnEmailTapped:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {
        NSString *strEmailAddress = [NSString stringWithFormat:@"%@",adminEmailAddress];
        // Email Subject
        NSString *emailTitle = @"";
        // Email Content
        NSString *messageBody = @"";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:strEmailAddress];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)btnAppointmentTapped:(id)sender {
    
    AppointmentViewController *pdvc=[self.storyboard instantiateViewControllerWithIdentifier:@"AppointmentViewController"];
    [self.navigationController pushViewController:pdvc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)offerLinkClicked:(id)sender {
    NSString *strUrl=[NSString stringWithFormat:@"%@",_dictOfferData[@"OfferUrl"]];
    NSURL *url=[NSURL URLWithString:strUrl];
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:url];
    svc.delegate = self;
    [self presentViewController:svc animated:YES completion:nil];

}
#pragma mark - Safari View Controller delegate methods
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
    
}
@end
