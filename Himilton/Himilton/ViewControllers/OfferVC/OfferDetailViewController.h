//
//  OfferDetailViewController.h
//  Himilton
//
//  Created by Sumit Sharma on 6/22/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferDetailViewController : UIViewController
@property (nonatomic,weak)NSMutableDictionary *dictOfferData;
@end
