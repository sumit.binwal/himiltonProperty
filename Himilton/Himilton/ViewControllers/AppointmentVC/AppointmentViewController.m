//
//  AppointmentViewController.m
//  Himilton
//
//  Created by Sumit Sharma on 6/17/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "AppointmentViewController.h"
#import <MessageUI/MessageUI.h>

@interface AppointmentViewController ()<MFMailComposeViewControllerDelegate>
{
    IBOutlet UITextField *txtFldFirstName;
    IBOutlet UITextField *txtFldLastName;
    IBOutlet UITextField *txtFldEmail;
    IBOutlet UITextField *txtFldPhoneNumber;
    IBOutlet UITextField *txtFldDateTime;
    
}
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@end

@implementation AppointmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Appointment"];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    [self.btnSubmit setBackgroundColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0]];
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    [borderLayer setLineWidth:1.0];
    CGRect bounds = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.btnSubmit.bounds.size.height);
    [borderLayer setFrame:bounds];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(20.0, 20.0)];
    borderLayer.path = path.CGPath;
    [self.btnSubmit.layer addSublayer:borderLayer];
    [self.btnSubmit.layer setMasksToBounds:false];
    [self.btnSubmit.layer setMask:borderLayer];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)submitButtonTap:(id)sender
{
    
    if ([MFMailComposeViewController canSendMail]) {
        NSString *strEmailAddress = adminEmailAddress;
        // Email Subject
        NSString *emailTitle = @"Appointment";
        // Email Content
        NSString *messageBody = [NSString stringWithFormat:@"Appointment \n ---------------------------- \n First Name : %@\n Last Name : %@\n Email Address : %@\n Phone Number : %@, Preffered Date Time : %@",txtFldFirstName.text,txtFldLastName.text,txtFldEmail.text,txtFldPhoneNumber.text,txtFldDateTime.text];
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:strEmailAddress];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    

    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
