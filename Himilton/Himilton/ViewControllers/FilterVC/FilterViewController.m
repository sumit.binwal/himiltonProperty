//
//  FilterViewController.m
//  Himilton
//
//  Created by Rohit Sharma on 5/31/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "FilterViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "FilterTVCell.h"
#import "AutoLayoutHelpers.h"
#import "PropertieVC.h"

@interface FilterViewController () <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, FilterTVCellDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewBuyRent;
@property (weak, nonatomic) IBOutlet UITextField *txtfSearchKeyword;
@property (strong, nonatomic) UISegmentedControl *sgmtBuyRent;
@property (strong, nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) UIView *pickerViewInputView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

@property (nonnull, strong) NSArray *pickerViewItems;
@property (nonatomic, assign) BOOL isPickerViewVisible;
@property (nonatomic, assign) NSInteger currentSelectedIndex;
@property (nonatomic, strong) NSDictionary *filterListDetails;

- (IBAction)btnSearchTapped:(id)sender;

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"FilterTVCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FilterTVCell"];
//    self.filterDetail = [[NSMutableDictionary alloc] init];
    NSString *searchText = [self.filterDetail objectForKey:@"keywords"];
    [self.txtfSearchKeyword setText:searchText];
    [self updateUI];
    [self.filterDetail setObject:@"" forKey:@"bordering"];
    [self fetchFilterDetails];
    
    UIBarButtonItem *dismissBtn = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStyleDone target:self action:@selector(dismissBtnTapped)];
    [self.navigationItem setRightBarButtonItem:dismissBtn];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationItem setTitle:@"Filter"];

}
#pragma mark -

- (void)dismissBtnTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnBackTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSearchTapped:(id)sender {
    NSLog(@"%@",self.filterDetail);
    
//    [self dismissViewControllerAnimated:YES completion:^{
//        if (self.delegate && [self.delegate respondsToSelector:@selector(showResultViewWithFilter:)]) {
//            [self.delegate showResultViewWithFilter:self.filterDetail];
//        }
//    }];
    PropertieVC *propertyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PropertieVC"];
    [propertyVC setFilterDict:self.filterDetail];
    [propertyVC setNeedDismissButton:YES];
    [self.navigationController pushViewController:propertyVC animated:YES];
}

- (IBAction)btnCancelTapped:(id)sender {
    [self.txtfSearchKeyword setText:@""];
}

- (void)updateUI {
    //237,95,27
    [self createBuyRentSegmentControl];
    
    [self.btnSearch setBackgroundColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0]];
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    [borderLayer setLineWidth:1.0];
    CGRect bounds = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.btnSearch.bounds.size.height);
    [borderLayer setFrame:bounds];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(20.0, 20.0)];
    borderLayer.path = path.CGPath;
    [self.btnSearch.layer addSublayer:borderLayer];
    [self.btnSearch.layer setMasksToBounds:false];
    [self.btnSearch.layer setMask:borderLayer];
}

- (void)createBuyRentSegmentControl {
    NSArray *filters = @[@"Buy",@"Rent"];
    self.sgmtBuyRent = [[UISegmentedControl alloc] initWithItems:filters];
    [self.sgmtBuyRent setTintColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0]];
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[[UIColor blackColor]] forKeys:@[NSForegroundColorAttributeName]];
    [self.sgmtBuyRent setTitleTextAttributes:dict forState:UIControlStateNormal];
    NSDictionary *selectedDict = [NSDictionary dictionaryWithObjects:@[[UIColor whiteColor]] forKeys:@[NSForegroundColorAttributeName]];
    [self.sgmtBuyRent setTitleTextAttributes:selectedDict forState:UIControlStateSelected];
    
    [self.sgmtBuyRent addTarget:self action:@selector(buySaleSegmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.sgmtBuyRent setBackgroundImage:[UIImage imageNamed:@"filter_select_box_empty"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.sgmtBuyRent setBackgroundImage:[UIImage imageNamed:@"filter_select_box_color"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self.sgmtBuyRent setWidth:140.0 forSegmentAtIndex:0];
    [self.sgmtBuyRent setWidth:140.0 forSegmentAtIndex:1];
    [self.sgmtBuyRent.layer setBorderColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0].CGColor];
    [self.sgmtBuyRent.layer setBorderWidth:1.0];
    [self.viewBuyRent addSubview:self.sgmtBuyRent];
    
    [self.viewBuyRent setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    constraintEqual(self.sgmtBuyRent, self.viewBuyRent, NSLayoutAttributeTop, 0.0);
    constraintEqual(self.sgmtBuyRent, self.viewBuyRent, NSLayoutAttributeBottom, 0.0);
    constraintEqual(self.sgmtBuyRent, self.viewBuyRent, NSLayoutAttributeLeading, 0.0);
    constraintEqual(self.sgmtBuyRent, self.viewBuyRent, NSLayoutAttributeTrailing, 0.0);
    constraintEqual(self.sgmtBuyRent, nil, NSLayoutAttributeWidth, 280.0);
    
    NSString *buySale = self.filterDetail[@"type"];
    if ([[buySale lowercaseString] isEqualToString:@"rent"]) {
        [self.sgmtBuyRent setSelectedSegmentIndex:1];
    } else {
        [self.sgmtBuyRent setSelectedSegmentIndex:0];
    }
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    CGRect bounds = CGRectMake(0.0, 0.0, self.sgmtBuyRent.frame.size.width, self.sgmtBuyRent.bounds.size.height);
    [maskLayer setFrame:bounds];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(20.0, 20.0)];
    maskLayer.path = path.CGPath;
    [self.sgmtBuyRent.layer setMask:maskLayer];
    
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    borderLayer.path = path.CGPath;
    [borderLayer setLineWidth:2.0];
    [borderLayer setFrame:bounds];
    [borderLayer setFillColor:[UIColor clearColor].CGColor];
    [borderLayer setStrokeColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0].CGColor];
    [self.sgmtBuyRent.layer addSublayer:borderLayer];
    [self.sgmtBuyRent.layer setMasksToBounds:false];
    
    [self addPickerView];
}

- (void)buySaleSegmentValueChanged:(id)sender {
    NSInteger index = [self.sgmtBuyRent selectedSegmentIndex];
    if (index == 0) {
        self.filterDetail[@"type"] = @"Buy";
    }
    else {
        self.filterDetail[@"type"] = @"rent";
    }
}

- (void)addPickerView {
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height + 200, self.view.frame.size.width, 200)];
    [self.pickerView setDelegate:self];
    [self.pickerView setDataSource:self];
    [self.pickerView setBackgroundColor:[UIColor whiteColor]];
    [self.pickerView setShowsSelectionIndicator:YES];
    [self.view addSubview:self.pickerView];
    [self addInputViewForPickerView];
}

- (void)addInputViewForPickerView {
    self.pickerViewInputView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    [self.pickerViewInputView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.pickerViewInputView];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, (self.pickerViewInputView.frame.size.height - 1.0), self.view.bounds.size.width, 1)];
    [separatorView setBackgroundColor:[UIColor grayColor]];
    [self.pickerViewInputView addSubview:separatorView];
    
    CGFloat buttonHeight = 36.0;
    CGFloat buttonWidth = 80;
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10.0, ((self.pickerViewInputView.frame.size.height - buttonHeight) * 0.5), buttonWidth, buttonHeight)];
    [cancelButton setBackgroundColor:[UIColor whiteColor]];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithRed:33/255.0 green:37/255.0 blue:63/255.0 alpha:1.0] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(pickerViewCancelBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerViewInputView addSubview:cancelButton];
    cancelButton.layer.cornerRadius = 5.0;
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake((self.pickerViewInputView.frame.size.width - 10 - buttonWidth), ((self.pickerViewInputView.frame.size.height - buttonHeight) * 0.5), buttonWidth, buttonHeight)];
    [doneButton setBackgroundColor:[UIColor whiteColor]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:33/255.0 green:37/255.0 blue:63/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(pickerViewDoneBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerViewInputView addSubview:doneButton];
    doneButton.layer.cornerRadius = 5.0;
}

- (void)pickerViewCancelBtnTapped:(UIButton *)sender {
    [self hidePickerView];
}

- (void)pickerViewDoneBtnTapped:(UIButton *)sender {
    NSInteger selectedRow = [self.pickerView selectedRowInComponent:0];
    NSString *itemSelected = [self.pickerViewItems objectAtIndex:selectedRow];
    
    FilterTVCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentSelectedIndex inSection:0]];
    [cell updatePriceOrPropertyValue:itemSelected];
    
    [self hidePickerView];
}

- (void)showPickerView {
    if (self.isPickerViewVisible) {
        return;
    }
    [self.pickerView setHidden:NO];
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = self.pickerView.frame;
        frame.origin.y = self.view.frame.size.height - frame.size.height;
        self.pickerView.frame = frame;
        
        CGRect inputViewFrame = self.pickerViewInputView.frame;
        inputViewFrame.origin.y = self.view.frame.size.height - frame.size.height - inputViewFrame.size.height;
        self.pickerViewInputView.frame = inputViewFrame;
        
    } completion:^(BOOL finished) {
        if (finished) {
            self.isPickerViewVisible = YES;
        }
    }];
}

- (void)hidePickerView {
    if (!self.isPickerViewVisible) {
        return;
    }
    [self.pickerView setHidden:YES];
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = self.pickerView.frame;
        frame.origin.y = self.view.frame.size.height + 40.0;
        self.pickerView.frame = frame;
        
        CGRect inputViewFrame = self.pickerViewInputView.frame;
        inputViewFrame.origin.y = self.view.frame.size.height;
        self.pickerViewInputView.frame = inputViewFrame;
        
    } completion:^(BOOL finished) {
        if (finished) {
            self.isPickerViewVisible = NO;
        }
    }];
}

- (void)showPickerViewWithtems:(NSArray *)items {
    self.pickerViewItems = items;
    [self.pickerView reloadAllComponents];
    [self showPickerView];
}

#pragma mark - 

- (void)fetchFilterDetails {
    
    [UtilityClass showHud];
    NSURL *apiURL=[NSURL URLWithString:API_FILTER_LIST];
    NSDictionary *parameter=@{
                              @"key":SERVICE_KEY
                              };
    
    __weak typeof(self)weakSelf = self;
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        [UtilityClass hideHud];
        if (statusCode==200) {
            
            self.filterListDetails = [dictionary[@"data"]mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
        else
        {
            if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            else
            {
                [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
        }
    } failure:^(NSError *error) {
        [UtilityClass hideHud];
        [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
    }];
}

- (NSArray *)filterValues:(NSInteger)row {
    NSArray *filters = nil;
    switch (row) {
        case 0:{
//            NSMutableArray *arrFilters = [NSMutableArray arrayWithArray:self.filterListDetails[@"prices"]];
//            NSArray* arrFilters = @[@"Any",@"$50,000-$75,000",@"$75,001-$100,000",@"$100,001-$125,000",@"$125,001-$150,000",@"$150,001-$175,000",@"$175,001-$200,000",@"$200,001-$225,000",@"$225,001-$250,000",@"$250,001-$275,000",@"$275,001 - $300,000",@"$300,001 - $325,000",@"$325,001 - $350,000",@"$350,001 - $375,000",@"$375,001 - $400,000"];
            NSMutableArray *arrFilterDummy=[NSMutableArray new];
            [arrFilterDummy addObject:@"Any"];
            
            for (int i = 50000; i<15000000; ) {
                int firstValue = i;
                
                int secondValue = firstValue+25000;
                
                
                NSString *display1 = [NSNumberFormatter localizedStringFromNumber:@(firstValue+1)numberStyle:NSNumberFormatterCurrencyAccountingStyle];
                
                display1 = [display1 stringByReplacingOccurrencesOfString:@".00" withString:@""];
                
                NSString *display2 = [NSNumberFormatter localizedStringFromNumber:@(secondValue)numberStyle:NSNumberFormatterCurrencyAccountingStyle];
                display2 = [display2 stringByReplacingOccurrencesOfString:@".00" withString:@""];
                
                NSString *actualString=[NSString stringWithFormat:@"%@-%@",display1,display2];
                [arrFilterDummy addObject:actualString];

                i=secondValue;
            }
            
            NSArray* arrFilters=[[NSArray alloc]initWithArray:arrFilterDummy];
            filters = arrFilters;
            //.... till $15,000,000 and last option $15,000,000+
        }
            break;
        case 1:{
            NSMutableArray *arrFilters = [NSMutableArray arrayWithArray:self.filterListDetails[@"propertyType"]];
            [arrFilters removeObject:@""];
            [arrFilters insertObject:@"Any" atIndex:0];
            filters = arrFilters;
        }
            break;
        case 2:
        {
            NSArray* arrFilters = @[@"Any",@"100-200",@"201-300",@"301-400",@"401-500",@"501-600",@"601-700",@"701-800",@"801-900",@"901-1000",@"1001-2000",@"2001-3000",@"3001-4000",@"4001-5000"                        ,@"5000+"];
            filters = arrFilters;
        }
            break;
        case 3:
        {
            //NSMutableArray *arrFilters = [NSMutableArray arrayWithArray:self.filterListDetails[@"badrooms"]];
            //[arrFilters removeObject:@""];
            NSArray *arrFilters = @[@"1",@"2",@"3",@"4",@"5"];
            
            filters = arrFilters;
        }
            break;
        case 4:
        {
//            NSMutableArray *arrFilters = [NSMutableArray arrayWithArray:self.filterListDetails[@"bathrooms"]];
            NSArray *arrFilters = @[@"1",@"2",@"3",@"4",@"5"];

//            [arrFilters removeObject:@""];
            filters = arrFilters;
        }
            break;
        
        case 5:
        {
//            NSMutableArray *arrFilters = [NSMutableArray arrayWithArray:self.filterListDetails[@"toilet"]];
//            [arrFilters removeObject:@""];
//            [arrFilters removeObject:@"Rajasthan"];
            NSArray *arrFilters = @[@"1",@"2",@"3",@"4",@"5"];

            filters = arrFilters;
        }
            break;
        case 6:
        {
//            NSMutableArray *arrFilters = [NSMutableArray arrayWithArray:self.filterListDetails[@"cars"]];
//            [arrFilters removeObject:@""];
            NSArray *arrFilters = @[@"1",@"2",@"3",@"4",@"5"];

            filters = arrFilters;
        }
            break;
        default:
            break;
    }
    return filters;
}

- (NSString *)filterName:(NSInteger)row {
    NSString *filterName = nil;
    switch (row) {
        case 0:
            filterName = @"Price Range";
            break;
        case 1:
            filterName = @"Property Type";
            break;
        case 2:
            filterName = @"Land Size";
            break;
        case 3:
            filterName = @"Bedrooms";
            break;
        case 4:
            filterName = @"Bathrooms";
            break;
        
        case 5:
            filterName = @"Toilets";
            break;
        case 6:
            filterName = @"Car Parks";
            break;
        case 7:
            filterName = @"NearbySuburbs";
            break;
        case 8:
            filterName = @"Keywords";
            break;
        default:
            break;
    }
    return filterName;
}

#pragma mark - TableView Delegate and DataSource Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 7 || indexPath.row == 8 ) {
        return 50.0;
    }
    else {
        return 90.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.filterListDetails) {
        return 9;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FilterTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterTVCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
        [cell setCellType:StaticLabel];
    } else if (indexPath.row == 7) {    //Nearby
        [cell setCellType:Nearby];
    } else if (indexPath.row == 8) {    //Keywords
        [cell setCellType:Keyword];
    }
    else {
        [cell setCellType:Option];
    }
    [cell setDelegate:self];
    
    NSArray *filters = [self filterValues:indexPath.row];
    NSString *filterName = [self filterName:indexPath.row];
    [cell setUpUIForDict:filters andFilterName:filterName];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
        self.currentSelectedIndex = indexPath.row;
        NSArray *filters = [self filterValues:indexPath.row];
        
        
        [self showPickerViewWithtems:filters];
    }
}

#pragma mark - PickerView Delegate and DataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.pickerViewItems.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title = self.pickerViewItems[row];
    return title;
}

#pragma mark - TextField Delegate Method 

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.filterDetail setObject:self.txtfSearchKeyword.text forKey:@"keywords"];
}

#pragma mark - FilterTV Cell Delegate

- (void)updateValueForKey:(NSString *)item withValue:(NSString *)filterValue {
    
    
    if ([self.filterDetail.allKeys containsObject:@"residential"] == NO) {
        [self.filterDetail setObject:@"" forKey:@"residential"];
    }
    
    if (self.sgmtBuyRent.selectedSegmentIndex == 0) {
        [self.filterDetail setObject:@"buy" forKey:@"type"];
    } else {
        [self.filterDetail setObject:@"rent" forKey:@"type"];
    }
    
    if ([item isEqualToString:@"Property Type"]) {
        if ([filterValue isEqualToString:@"Any"]) {
            [self.filterDetail setObject:@"" forKey:@"alltype"];
        }
        else
        {
        [self.filterDetail setObject:[filterValue lowercaseString] forKey:@"alltype"];
        }
        
    }
    else if ([item isEqualToString:@"Bedrooms"]) {
        if ([filterValue isEqualToString:@"Any"]) {
            [self.filterDetail setObject:@"" forKey:@"bed"];
        }
        else
        {
        [self.filterDetail setObject:filterValue forKey:@"bed"];
        }
    }
    else if ([item isEqualToString:@"Bathrooms"]) {
        if ([filterValue isEqualToString:@"Any"]) {
            [self.filterDetail setObject:@"" forKey:@"bath"];
        }
        else
        {
        [self.filterDetail setObject:filterValue forKey:@"bath"];
        }
    }
    else if ([item isEqualToString:@"Car Parks"]) {
        if ([filterValue isEqualToString:@"Any"]) {
            [self.filterDetail setObject:@"" forKey:@"car"];
        }
        else
        {
        [self.filterDetail setObject:filterValue forKey:@"car"];
        }
    }
    else if ([item isEqualToString:@"NearbySuburbs"]) {
        [self.filterDetail setObject:filterValue forKey:@"bordering"];
    }
    else if ([item isEqualToString:@"Price Range"]) {
        if ([filterValue isEqualToString:@"Any"])
        {
            [self.filterDetail setObject:@"" forKey:@"minprice"];
            [self.filterDetail setObject:@"" forKey:@"maxprice"];
            
        }
        else
        {
            NSArray *minMaxValue = [filterValue componentsSeparatedByString:@"-"];
            if (minMaxValue.count > 1) {
                NSString *minPrice = minMaxValue[0];
                minPrice = [minPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                minPrice = [minPrice stringByReplacingOccurrencesOfString:@"," withString:@""];
                minPrice = [minPrice stringByReplacingOccurrencesOfString:@"Any" withString:@""];
                
                NSString *maxPrice = minMaxValue[1];
                maxPrice = [maxPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                maxPrice = [maxPrice stringByReplacingOccurrencesOfString:@"," withString:@""];
                maxPrice = [maxPrice stringByReplacingOccurrencesOfString:@"Any" withString:@""];
                [self.filterDetail setObject:minPrice forKey:@"minprice"];
                [self.filterDetail setObject:maxPrice forKey:@"maxprice"];
            }else {
                NSString *maxPrice = minMaxValue[0];
                maxPrice = [maxPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                maxPrice = [maxPrice stringByReplacingOccurrencesOfString:@"," withString:@""];
                maxPrice = [maxPrice stringByReplacingOccurrencesOfString:@"Any" withString:@""];
                
                
                [self.filterDetail setObject:@"0" forKey:@"minprice"];
                [self.filterDetail setObject:maxPrice forKey:@"maxprice"];
            }

        }
        
            }
    else if ([item isEqualToString:@"Land Size"]) {
        if ([filterValue isEqualToString:@"Any"])
        {
            [self.filterDetail setObject:@"" forKey:@"landsize"];
            [self.filterDetail setObject:@"" forKey:@"landsizemax"];
        }
        else
        {
            NSArray *minMaxValue = [filterValue componentsSeparatedByString:@"-"];
            if (minMaxValue.count > 1) {
                NSString *minPrice = minMaxValue[0];
                NSString *maxPrice = minMaxValue[1];
                [maxPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                [minPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                
                [self.filterDetail setObject:minPrice forKey:@"landsize"];
                [self.filterDetail setObject:maxPrice forKey:@"landsizemax"];
            }else {
                NSString *maxPrice = minMaxValue[0];
                [maxPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                [self.filterDetail setObject:@"0" forKey:@"landsize"];
                [self.filterDetail setObject:maxPrice forKey:@"landsizemax"];
            }

        }
            }
    else if ([item isEqualToString:@"Toilets"]) {
        if ([filterValue isEqualToString:@"Any"]) {
            [self.filterDetail setObject:@"" forKey:@"toilet"];
        }
        else
        {
            [self.filterDetail setObject:filterValue forKey:@"toilet"];
        }
        
    }
    else if ([item isEqualToString:@"Keywords"]) {
        [self.filterDetail setObject:filterValue forKey:@"keywords"];
    }
}
@end
