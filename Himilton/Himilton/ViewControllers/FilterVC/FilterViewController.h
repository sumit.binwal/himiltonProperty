//
//  FilterViewController.h
//  Himilton
//
//  Created by Rohit Sharma on 5/31/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewControllerDelegate <NSObject>

- (void)showResultViewWithFilter:(NSMutableDictionary *)dict;

@end

@interface FilterViewController : UIViewController

@property (nonatomic, assign) NSMutableDictionary *filterDetail;
@property (nonatomic, assign) id<FilterViewControllerDelegate> delegate;

@end
