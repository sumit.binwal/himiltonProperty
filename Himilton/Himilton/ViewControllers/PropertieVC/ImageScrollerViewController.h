//
//  ImageScrollerViewController.h
//  Himilton
//
//  Created by Rohit Sharma on 5/9/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageScrollerViewController : UIViewController

@property (nonatomic, strong) NSArray *arrImagePaths;

@end
