//
//  ImageScrollerViewController.m
//  Himilton
//
//  Created by Rohit Sharma on 5/9/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

#import "ImageScrollerViewController.h"
#import "JOLImageSlide.h"
#import "JOLImageSlider.h"

@interface ImageScrollerViewController ()

@end

@implementation ImageScrollerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableArray *arrSlides = [[NSMutableArray alloc] init];
    for (NSString *strPath in self.arrImagePaths) {
        JOLImageSlide *slide = [[JOLImageSlide alloc] init];
        slide.image = strPath;
        [arrSlides addObject:slide];
    }
    
    JOLImageSlider *imageSlider = [[JOLImageSlider alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andSlides: arrSlides];
    [imageSlider initialize];
    //[imageSlider setAutoSlide: YES];
//    [imageSlider setPlaceholderImage:@"placeholder.png"];
    [imageSlider setContentMode: UIViewContentModeScaleAspectFill];
    [self.view addSubview: imageSlider];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
