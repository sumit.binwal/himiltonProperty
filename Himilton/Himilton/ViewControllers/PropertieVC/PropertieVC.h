//
//  PropertieVC.h
//  Himilton
//
//  Created by Sumit Sharma on 5/29/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PropertieVC : UIViewController

@property (nonatomic, strong) NSDictionary *filterDict;
@property (nonatomic, assign) BOOL needDismissButton;

@end
