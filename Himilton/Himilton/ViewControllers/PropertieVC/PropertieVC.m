//
//  PropertieVC.m
//  Himilton
//
//  Created by Sumit Sharma on 5/29/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "PropertieVC.h"
#import "PropertyDetailViewController.h"
#import "PropertieListCell.h"
#import "LoginVC.h"
#import "OfferViewController.h"
@interface PropertieVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSMutableArray *arrPropertie;
    IBOutlet UILabel *labelPropertyCOunt;
    IBOutlet UITableView *tblVwPropertieList;
}
@property (nonatomic, strong) NSArray *sortedProperties;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIPickerView *pickerViewInputView;
@property (nonatomic, strong) NSArray *pickerViewItems;
@property (nonatomic, assign) BOOL isPickerViewVisible;
@end

@implementation PropertieVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrPropertie=[NSMutableArray new];
    if (self.needDismissButton) {
        UIBarButtonItem *dismissBtn = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStyleDone target:self action:@selector(dismissBtnTapped)];
        [self.navigationItem setRightBarButtonItem:dismissBtn];
    }
    self.pickerViewItems = @[@"High to Low",@"Low to High",@"Random"];
    [self addPickerView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = @"Properties";
    if (self.needDismissButton) {
        self.navigationController.navigationBar.topItem.title = @"Filter";

    }
    [self fetchPropertieListingFromServerAPI];
    
    tblVwPropertieList.estimatedRowHeight= 264 * scaleFactorX;;
    tblVwPropertieList.rowHeight=UITableViewAutomaticDimension;
}
-(void)viewWillDisappear:(BOOL)animated
{
   // self.filterDict=nil;
}

#pragma mark -

- (void)dismissBtnTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSortTapped:(id)sender {
    [self showPickerViewWithtems:self.pickerViewItems];
}

#pragma mark : DZNEmptyData DataSource And Delegates

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"logo"];
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Properties Found";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate method
//-(void)tableView:(UITableView *)tableView prefetchRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
//{
//    NSLog(@"%@",indexPaths);
//}
//-(void)tableView:(UITableView *)tableView cancelPrefetchingForRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
//{
//    NSLog(@"%@",indexPaths);
//}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sortedProperties.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;

}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"proptieList";
    
    PropertieListCell *cell = (PropertieListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.labelBed.text=[NSString stringWithFormat:@"%@",self.sortedProperties[indexPath.row][@"No_of_Badrooms"]];
    cell.labelCar.text=[NSString stringWithFormat:@"%@",self.sortedProperties[indexPath.row][@"No_of_cars"]];
    cell.labelBath.text=[NSString stringWithFormat:@"%@",self.sortedProperties[indexPath.row][@"No_of_Bathrooms"]];
    if ([self.sortedProperties[indexPath.row][@"Address"] isKindOfClass:[NSString class]]) {
        cell.labelAddress.text=[NSString stringWithFormat:@"%@",self.sortedProperties[indexPath.row][@"Address"]];
    }else if ([self.sortedProperties[indexPath.row][@"Address"] isKindOfClass:[NSArray class]]){
        cell.labelAddress.text=[NSString stringWithFormat:@"%@",self.sortedProperties[indexPath.row][@"Address"][@"address"]];
    }
    
    cell.labelAddress.numberOfLines=0;
    
    NSString *displayStr = [NSNumberFormatter localizedStringFromNumber:@([self.sortedProperties[indexPath.row][@"price"] integerValue])
                                                         numberStyle:NSNumberFormatterCurrencyAccountingStyle];

    displayStr = [displayStr stringByReplacingOccurrencesOfString:@".00" withString:@""];
    
    cell.labelPrice.text=[NSString stringWithFormat:@"%@",displayStr];
    
    [cell.btnOurOffer addTarget:self action:@selector(ourOfferBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    id propertyImage = nil;
    if ([self.sortedProperties[indexPath.row][@"PropertyImage"] isKindOfClass:[NSString class]]) {
        propertyImage = self.sortedProperties[indexPath.row][@"PropertyImage"];
    } else if ([self.sortedProperties[indexPath.row][@"PropertyImage"] isKindOfClass:[NSArray class]]) {
        NSArray *images = self.sortedProperties[indexPath.row][@"PropertyImage"];
        propertyImage = images[0];
        
        NSString *strUrl=[NSString stringWithFormat:@"%@",images[0]];
        if (strUrl.length<7) {
            propertyImage=nil;
        }
    }
    if (propertyImage != nil) {
        [cell.imgVwPropertie setImageWithURL:[NSURL URLWithString:propertyImage] placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    cell.buttonImageLike.tag=indexPath.row;
    
    [cell.buttonImageLike addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    if ([self.sortedProperties[indexPath.row][@"islike"]boolValue]) {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
        
    }
    else
    {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
    }
    return cell;
}

-(IBAction)likeButtonClicked:(UIButton *)sender
{
    if (![UtilityClass getUserInfoDictionary])
    {
        [UtilityClass showAlertWithTitle:APP_NAME message:@"To use this feature, plesae login first" onViewController:self withButtonsArray:@[@"Cancel",@"OK"] dismissBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex==1) {
                LoginVC *lvc=[self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:lvc animated:YES completion:nil];            }
        }];
        return;
    }

    
    PropertieListCell *cell=(PropertieListCell *)[tblVwPropertieList cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    NSURL *apiURL;

    if ([self.sortedProperties[sender.tag][@"islike"]boolValue]) {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
        
        apiURL=[NSURL URLWithString:API_UNLIKE];

        NSMutableArray *fullDIct=[self.sortedProperties mutableCopy];
        NSMutableDictionary *tempDict=[fullDIct[sender.tag]mutableCopy];
        [tempDict setObject:@"0" forKey:@"islike"];
        [fullDIct replaceObjectAtIndex:sender.tag withObject:tempDict];
        self.sortedProperties =fullDIct;
    }
    else
    {
        [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
        apiURL=[NSURL URLWithString:API_LIKE];

        NSMutableArray *fullDIct=[self.sortedProperties mutableCopy];
        NSMutableDictionary *tempDict=[fullDIct[sender.tag]mutableCopy];
        [tempDict setObject:@"1" forKey:@"islike"];
        [fullDIct replaceObjectAtIndex:sender.tag withObject:tempDict];
        self.sortedProperties =fullDIct;
    }
    
    
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    
    [parameter setObject:SERVICE_KEY forKey:@"key"];
    [parameter setObject:[UtilityClass getUserInfoLoginTokenValue] forKey:@"userId"];
    [parameter setObject:self.sortedProperties[sender.tag][@"PropertyId"] forKey:@"propertyId"];
    __weak typeof(self)weakSelf = self;
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        [UtilityClass hideHud];
        if (statusCode==200) {
            
            
            
        }
        else
        {
            if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            else
            {
                [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            
            if ([self.sortedProperties[sender.tag][@"islike"]boolValue]) {
                [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
                
                
                NSMutableArray *fullDIct=[self.sortedProperties mutableCopy];
                NSMutableDictionary *tempDict=[fullDIct[sender.tag]mutableCopy];
                [tempDict setObject:@"0" forKey:@"islike"];
                [fullDIct replaceObjectAtIndex:sender.tag withObject:tempDict];
                self.sortedProperties =fullDIct;
            }
            else
            {
                [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
                
                NSMutableArray *fullDIct=[self.sortedProperties mutableCopy];
                NSMutableDictionary *tempDict=[fullDIct[sender.tag]mutableCopy];
                [tempDict setObject:@"1" forKey:@"islike"];
                [fullDIct replaceObjectAtIndex:sender.tag withObject:tempDict];
                self.sortedProperties =fullDIct;
            }
        }
    } failure:^(NSError *error) {
        [UtilityClass hideHud];
        [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
        
        if ([self.sortedProperties[sender.tag][@"islike"]boolValue]) {
            [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
            
            NSMutableArray *fullDIct=[self.sortedProperties mutableCopy];
            NSMutableDictionary *tempDict=[fullDIct[sender.tag]mutableCopy];
            [tempDict setObject:@"0" forKey:@"islike"];
            [fullDIct replaceObjectAtIndex:sender.tag withObject:tempDict];
            self.sortedProperties =fullDIct;
        }
        else
        {
            [cell.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
            
            NSMutableArray *fullDIct=[self.sortedProperties mutableCopy];
            NSMutableDictionary *tempDict=[fullDIct[sender.tag]mutableCopy];
            [tempDict setObject:@"1" forKey:@"islike"];
            [fullDIct replaceObjectAtIndex:sender.tag withObject:tempDict];
            self.sortedProperties =fullDIct;
        }
    }];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
    PropertyDetailViewController *pdvc=[self.storyboard instantiateViewControllerWithIdentifier:@"propertyDetailVC"];
    pdvc.hidesBottomBarWhenPushed=YES;
    pdvc.dictData=self.sortedProperties[indexPath.row];
    [pdvc setNeedDismissButton:self.needDismissButton];
    [self.navigationController pushViewController:pdvc animated:YES];
}

- (IBAction)ourOfferBtnAction:(id)sender
{
    OfferViewController *offerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"offerVC"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:offerVC];
    
    [self presentViewController:navController animated:YES completion:nil];
}



#pragma mark -
#pragma mark : TableView Cell Custome Method - Get Propertie Listing Action
-(void)fetchPropertieListingFromServerAPI
{
    
    [UtilityClass showHud];
    NSURL *apiURL;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    if (self.filterDict) {
        [parameter setDictionary:self.filterDict];
        apiURL=[NSURL URLWithString:API_PROPERTIE_FILTER];

    }
    else
    {
        apiURL=[NSURL URLWithString:API_PROPERTIE_LIST];

    }
    
    if (![UtilityClass getUserInfoDictionary])
    {
        [parameter setObject:SERVICE_KEY forKey:@"key"];
    }
    else
    {
        NSLog(@"%@",[UtilityClass getUserInfoDictionary]);
        [parameter setObject:SERVICE_KEY forKey:@"key"];
        [parameter setObject:[UtilityClass getUserInfoLoginTokenValue] forKey:@"userId"];
    }
    
    __weak typeof(self)weakSelf = self;
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        [UtilityClass hideHud];
        if (statusCode==200) {
            arrPropertie=[dictionary [@"data"]mutableCopy];
            self.sortedProperties = [arrPropertie sortedArrayUsingComparator:^(NSMutableDictionary *obj1,NSMutableDictionary *obj2) {
                NSInteger first =[[obj1 objectForKey:@"price"] integerValue];
                NSInteger second =[[obj2 objectForKey:@"price"] integerValue];
                
                if ( first < second ) {
                    return (NSComparisonResult)NSOrderedAscending;
                } else if ( first > second ) {
                    return (NSComparisonResult)NSOrderedDescending;
                } else {
                    return (NSComparisonResult)NSOrderedSame;
                }
            }];
            labelPropertyCOunt.text=[NSString stringWithFormat:@"%lu of %lu Properties",(unsigned long)arrPropertie.count,(unsigned long)arrPropertie.count];
            [tblVwPropertieList reloadData];
//            [arrPendingData removeObjectAtIndex:sender.tag];
//            [_tableViewPendingRequest beginUpdates];
//            
//            [_tableViewPendingRequest deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:sender.tag inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
//            [_tableViewPendingRequest endUpdates];
            
            //If No Record In Array Showing Text
            
        }
               else
        {
            if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
            else
            {
                [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            }
        }
    } failure:^(NSError *error) {
        [UtilityClass hideHud];
        [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - PickerView Methods

- (void)addPickerView {
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height + 200, self.view.frame.size.width, 200)];
    [self.pickerView setDelegate:self];
    [self.pickerView setDataSource:self];
    [self.pickerView setBackgroundColor:[UIColor whiteColor]];
    [self.pickerView setShowsSelectionIndicator:YES];
    [self.view addSubview:self.pickerView];
    [self addInputViewForPickerView];
}

- (void)addInputViewForPickerView {
    self.pickerViewInputView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    [self.pickerViewInputView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.pickerViewInputView];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, (self.pickerViewInputView.frame.size.height - 1.0), self.view.bounds.size.width, 1)];
    [separatorView setBackgroundColor:[UIColor grayColor]];
    [self.pickerViewInputView addSubview:separatorView];
    
    CGFloat buttonHeight = 36.0;
    CGFloat buttonWidth = 80;
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10.0, ((self.pickerViewInputView.frame.size.height - buttonHeight) * 0.5), buttonWidth, buttonHeight)];
    [cancelButton setBackgroundColor:[UIColor whiteColor]];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithRed:33/255.0 green:37/255.0 blue:63/255.0 alpha:1.0] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(pickerViewCancelBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerViewInputView addSubview:cancelButton];
    cancelButton.layer.cornerRadius = 5.0;
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake((self.pickerViewInputView.frame.size.width - 10 - buttonWidth), ((self.pickerViewInputView.frame.size.height - buttonHeight) * 0.5), buttonWidth, buttonHeight)];
    [doneButton setBackgroundColor:[UIColor whiteColor]];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor colorWithRed:33/255.0 green:37/255.0 blue:63/255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(pickerViewDoneBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickerViewInputView addSubview:doneButton];
    doneButton.layer.cornerRadius = 5.0;
}

- (void)pickerViewCancelBtnTapped:(UIButton *)sender {
    [self hidePickerView];
}

- (void)pickerViewDoneBtnTapped:(UIButton *)sender {
    NSInteger selectedRow = [self.pickerView selectedRowInComponent:0];
    
    if (selectedRow == 0) {
        self.sortedProperties = [arrPropertie sortedArrayUsingComparator:^(NSMutableDictionary *obj1,NSMutableDictionary *obj2) {
            NSInteger first =[[obj1 objectForKey:@"price"] integerValue];
            NSInteger second =[[obj2 objectForKey:@"price"] integerValue];
            
            if ( first > second ) {
                return (NSComparisonResult)NSOrderedAscending;
            } else if ( first < second ) {
                return (NSComparisonResult)NSOrderedDescending;
            } else {
                return (NSComparisonResult)NSOrderedSame;
            }
        }];
    }
    else if (selectedRow == 1) {
        self.sortedProperties = [arrPropertie sortedArrayUsingComparator:^(NSMutableDictionary *obj1,NSMutableDictionary *obj2) {
            NSInteger first =[[obj1 objectForKey:@"price"] integerValue];
            NSInteger second =[[obj2 objectForKey:@"price"] integerValue];
            
            if ( first < second ) {
                return (NSComparisonResult)NSOrderedAscending;
            } else if ( first > second ) {
                return (NSComparisonResult)NSOrderedDescending;
            } else {
                return (NSComparisonResult)NSOrderedSame;
            }
        }];
    }
    else {
        self.sortedProperties = arrPropertie;
    }
    [tblVwPropertieList reloadData];
    
    [self hidePickerView];
}

- (void)showPickerView {
    if (self.isPickerViewVisible) {
        return;
    }
    [self.pickerView setHidden:NO];
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = self.pickerView.frame;
        frame.origin.y = self.view.frame.size.height - frame.size.height;
        self.pickerView.frame = frame;
        
        CGRect inputViewFrame = self.pickerViewInputView.frame;
        inputViewFrame.origin.y = self.view.frame.size.height - frame.size.height - inputViewFrame.size.height;
        self.pickerViewInputView.frame = inputViewFrame;
        
    } completion:^(BOOL finished) {
        if (finished) {
            self.isPickerViewVisible = YES;
        }
    }];
}

- (void)hidePickerView {
    if (!self.isPickerViewVisible) {
        return;
    }
    [self.pickerView setHidden:YES];
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = self.pickerView.frame;
        frame.origin.y = self.view.frame.size.height + 40.0;
        self.pickerView.frame = frame;
        
        CGRect inputViewFrame = self.pickerViewInputView.frame;
        inputViewFrame.origin.y = self.view.frame.size.height;
        self.pickerViewInputView.frame = inputViewFrame;
        
    } completion:^(BOOL finished) {
        if (finished) {
            self.isPickerViewVisible = NO;
        }
    }];
}

- (void)showPickerViewWithtems:(NSArray *)items {
//    self.pickerViewItems = items;
    [self.pickerView reloadAllComponents];
    [self showPickerView];
}

#pragma mark - PickerView Delegate and DataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.pickerViewItems.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title = self.pickerViewItems[row];
    return title;
}

@end
