//
//  PropertyDetailViewController.h
//  Himilton
//
//  Created by Rohit Sharma on 5/30/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PropertyDetailViewController : UIViewController

@property(nonatomic,weak)NSMutableDictionary *dictData;
@property (nonatomic, assign) BOOL needDismissButton;

@end
