//
//  PropertyDetailViewController.m
//  Himilton
//
//  Created by Rohit Sharma on 5/30/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "PropertyDetailViewController.h"
#import <MessageUI/MessageUI.h>
#import "LoginVC.h"
#import "AppointmentViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ImageScrollerViewController.h"
#import <Himilton-Swift.h>

@interface PropertyDetailViewController ()<MFMailComposeViewControllerDelegate, UIScrollViewDelegate>
{
    UIButton *barBtnLike;
}
@property (strong, nonatomic) IBOutlet UIView *vwFloorPlan;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewFullImage;
@property (strong, nonatomic) IBOutlet UIView *vwFullImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imgvProperty;
@property (weak, nonatomic) IBOutlet UILabel *lblBedroomCount;
@property (strong, nonatomic) IBOutlet UIImageView *imageVwFloorImage;
@property (weak, nonatomic) IBOutlet UILabel *lblBathroomCount;
@property (strong, nonatomic) IBOutlet GMSMapView *viewMapView;
@property (weak, nonatomic) IBOutlet UILabel *lblParkingCount;
@property (weak, nonatomic) IBOutlet UIImageView *imgvFavourite;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferValue;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblFeatures;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwLike;
@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroller;
@property (strong, nonatomic) IBOutlet UIButton *floorButton;


@end

@implementation PropertyDetailViewController
- (IBAction)floorButtonAction:(id)sender {
    [_vwFullImageView setFrame:[UIScreen mainScreen].bounds];
    NSURL *floorImageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.dictData[@"floorPlanMap"][@"large"]]];

    [_imageViewFullImage setImageWithURL:floorImageUrl placeholderImage:_imageVwFloorImage.image usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.navigationController.view.window addSubview:_vwFullImageView];
}
- (IBAction)closeButtonClicked:(id)sender {
    [_vwFullImageView removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"Back";

    [self setUpView];
    [self setupGoogleMap];
    NSLog(@"%@",[UtilityClass getUserInfoDictionary]);
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    CGRect bounds = CGRectMake(0.0, 0.0, _vwFloorPlan.frame.size.width - 10, _vwFloorPlan.bounds.size.height);
    [maskLayer setFrame:bounds];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(20.0, 20.0)];
    maskLayer.path = path.CGPath;
    [_vwFloorPlan.layer setMask:maskLayer];
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                          initWithTarget:self
                                                          action:@selector(showImageScroller:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [self.imageScroller addGestureRecognizer:singleTapGestureRecognizer];

    // Do any additional setup after loading the view.
}

- (void)dismissBtnTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"ImageScrollerScene"]) {
        NSArray *arrPaths = nil;
        if ([self.dictData[@"PropertyImage"] isKindOfClass:[NSString class]]) {
            arrPaths = self.dictData[@"PropertyImage"];
        } else if ([self.dictData[@"PropertyImage"] isKindOfClass:[NSArray class]]) {
            arrPaths = self.dictData[@"PropertyImage"];
        }
        ImageScrollerViewController *scroller = [segue destinationViewController];
        scroller.arrImagePaths = arrPaths;
    }
}

#pragma mark : Map Initial Setup
- (void)setupGoogleMap
{
    float latitude = 0.0;
    float longitude = 0.0;
    if ([self.dictData[@"Address"] isKindOfClass:[NSArray class]]) {
        latitude = [self.dictData[@"Address"][@"lat"]floatValue];
        longitude =[self.dictData[@"Address"][@"lng"]floatValue];
    }
    
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:16];
        
      [_viewMapView setCamera:camera];
    
    GMSMarker *dropMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude, longitude)];
    dropMarker.map = _viewMapView;

    
}

-(void)setUpView
{
    
    //Add Share and like Button
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *btnShare = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareButtonClicked)];
    
    if (self.needDismissButton) {
        UIBarButtonItem *dismissBtn = [[UIBarButtonItem alloc] initWithTitle:@"Dismiss" style:UIBarButtonItemStyleDone target:self action:@selector(dismissBtnTapped)];
        [arrRightBarItems addObject:dismissBtn];
    }

    
    [arrRightBarItems addObject:btnShare];
    
    barBtnLike = [UIButton buttonWithType:UIButtonTypeCustom];
    barBtnLike.frame = CGRectMake(0, 0, 32, 32);
    barBtnLike.showsTouchWhenHighlighted=YES;
    [barBtnLike addTarget:self action:@selector(likeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:barBtnLike];
    [arrRightBarItems addObject:barButtonItem];
    
    //Fill The Data
    NSString *displayStr = [NSNumberFormatter localizedStringFromNumber:@([self.dictData[@"price"] integerValue])
                                                            numberStyle:NSNumberFormatterCurrencyAccountingStyle];
    
    displayStr = [displayStr stringByReplacingOccurrencesOfString:@".00" withString:@""];
    
    

    self.lblBedroomCount.text=[NSString stringWithFormat:@"%@",self.dictData[@"No_of_Badrooms"]];
    self.lblParkingCount.text=[NSString stringWithFormat:@"%@",self.dictData[@"No_of_cars"]];
    self.lblBathroomCount.text=[NSString stringWithFormat:@"%@",self.dictData[@"No_of_Bathrooms"]];
    
    if ([self.dictData[@"Address"] isKindOfClass:[NSString class]]) {
        self.lblAddress.text=[self stringByStrippingHTML:[NSString stringWithFormat:@"%@",self.dictData[@"Address"]]];
    }else if ([self.dictData[@"Address"] isKindOfClass:[NSArray class]]){
        self.lblAddress.text=[self stringByStrippingHTML:[NSString stringWithFormat:@"%@",self.dictData[@"Address"][@"address"]]];
    }
    self.lblOfferValue.text=[NSString stringWithFormat:@"Offer Over %@",displayStr];
    self.lblFeatures.text=[self stringByStrippingHTML:[NSString stringWithFormat:@"%@",self.dictData[@"PropertyName"]]] ;
    self.lblDescription.text=[self stringByStrippingHTML:[NSString stringWithFormat:@"%@",self.dictData[@"PropertyDescription"]]];
    
    NSURL *floorImageUrl=[NSURL URLWithString:@""];
    if ([self.dictData[@"floorPlanMap"] isKindOfClass:[NSString class]]) {
        floorImageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.dictData[@"floorPlanMap"]]];
    }else if ([self.dictData[@"floorPlanMap"] isKindOfClass:[NSArray class]]){
        floorImageUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.dictData[@"floorPlanMap"][@"medium"]]];
    }
    [self.imageVwFloorImage setImageWithURL:floorImageUrl usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    id propertyImage = nil;
    if ([self.dictData[@"PropertyImage"] isKindOfClass:[NSString class]]) {
        propertyImage = self.dictData[@"PropertyImage"];
        NSString *image = (NSString *)propertyImage;
        [self addImageToScroller:@[image]];
    } else if ([self.dictData[@"PropertyImage"] isKindOfClass:[NSArray class]]) {
        NSArray *images = self.dictData[@"PropertyImage"];
        [self addImageToScroller:images];
    }

    if ([self.dictData[@"islike"]boolValue]) {
        [self.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
        [barBtnLike setImage:[UIImage imageNamed:@"likeIconFocus"] forState:UIControlStateNormal];


       barBtnLike.tag=1;
        _btnLike.tag=1;
    }
    else
    {

        [self.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
        [barBtnLike setImage:[UIImage imageNamed:@"likeIcon"] forState:UIControlStateNormal];

        barBtnLike.tag=0;
        _btnLike.tag=0;
    }
    
    
   
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
}

- (void)addImageToScroller:(NSArray *)images {
 
    [self.pageControl setNumberOfPages:images.count];
    [self.pageControl setCurrentPage:0];
    [self.imageScroller setDelegate:self];
    [self.pageControl setUserInteractionEnabled:NO];
    
    CGFloat offsetX = 0;
    for (int i = 0; i< images.count; i++) {
        NSString *imageUrl = images[i];
        UIImageView *imgvProperty = [[UIImageView alloc] initWithFrame:CGRectMake(offsetX, 0, self.view.frame.size.width, self.imageScroller.frame.size.height)];
        [imgvProperty setContentMode:UIViewContentModeScaleAspectFill];
        [imgvProperty setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@""] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.imageScroller addSubview:imgvProperty];
        offsetX += self.view.frame.size.width;
    }
    [self.imageScroller setContentSize:CGSizeMake(offsetX, self.imageScroller.frame.size.height)];
}

- (IBAction)likeButtonClicked:(UIButton *)sender {
    
    if (![UtilityClass getUserInfoDictionary])
    {
        [UtilityClass showAlertWithTitle:APP_NAME message:@"To use this feature, plesae login first" onViewController:self withButtonsArray:@[@"Cancel",@"OK"] dismissBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex==1) {
                LoginVC *lvc=[self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:lvc animated:YES completion:nil];            }
        }];
        return;
    }
    
    NSURL *apiURL;
    if (sender.tag==0) {
        [self.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
        [barBtnLike setImage:[UIImage imageNamed:@"likeIconFocus"] forState:UIControlStateNormal];
        apiURL=[NSURL URLWithString:API_LIKE];

        
        barBtnLike.tag=1;
        _btnLike.tag=1;
    }
    else
    {
        apiURL=[NSURL URLWithString:API_UNLIKE];

        [self.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
        [barBtnLike setImage:[UIImage imageNamed:@"likeIcon"] forState:UIControlStateNormal];
        
        barBtnLike.tag=0;
        _btnLike.tag=0;
    }
    
        NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
        
       
        [parameter setObject:SERVICE_KEY forKey:@"key"];
        [parameter setObject:[UtilityClass getUserInfoLoginTokenValue] forKey:@"userId"];
        [parameter setObject:self.dictData[@"PropertyId"] forKey:@"propertyId"];
        __weak typeof(self)weakSelf = self;
        
        [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
            
            [UtilityClass hideHud];
            if (statusCode==200) {


                
            }
            else
            {
                if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
                }
                else
                {
                    [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
                }
                
                if (sender.tag==1) {
                    [self.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
                    [barBtnLike setImage:[UIImage imageNamed:@"likeIconFocus"] forState:UIControlStateNormal];
                    barBtnLike.tag=1;
                    _btnLike.tag=1;
                }
                else
                {
                    [self.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
                    [barBtnLike setImage:[UIImage imageNamed:@"likeIcon"] forState:UIControlStateNormal];
                    barBtnLike.tag=0;
                    _btnLike.tag=0;
                }
            }
        } failure:^(NSError *error) {
            [UtilityClass hideHud];
            [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
            
            if (sender.tag==1) {
                [self.imgVwLike setImage:[UIImage imageNamed:@"likeIconFocus"]];
                [barBtnLike setImage:[UIImage imageNamed:@"likeIconFocus"] forState:UIControlStateNormal];
                barBtnLike.tag=1;
                _btnLike.tag=1;
            }
            else
            {
                [self.imgVwLike setImage:[UIImage imageNamed:@"likeIcon"]];
                [barBtnLike setImage:[UIImage imageNamed:@"likeIcon"] forState:UIControlStateNormal];
                barBtnLike.tag=0;
                _btnLike.tag=0;
            }
        }];
    
    
}

- (IBAction)showImageScroller:(id)sender {
    NSArray *arrPaths = nil;
    if ([self.dictData[@"PropertyImage"] isKindOfClass:[NSString class]]) {
        arrPaths = self.dictData[@"PropertyImage"];
    } else if ([self.dictData[@"PropertyImage"] isKindOfClass:[NSArray class]]) {
        arrPaths = self.dictData[@"PropertyImage"];
    }
    NSMutableArray *pictures = [[NSMutableArray alloc] init];
    for (NSString *paths in arrPaths) {
        CollieGalleryPicture *picture = [[CollieGalleryPicture alloc] initWithUrl:paths placeholder:nil title:nil caption:nil];
        [pictures addObject:picture];
    }
    CollieGallery *gallery = [[CollieGallery alloc] initWithPictures:pictures];
    [gallery presentInViewController:self];
    
//    [self performSegueWithIdentifier:@"ImageScrollerScene" sender:nil];
}


-(void)shareButtonClicked
{
    NSString *string = self.dictData[@"PropertyName"];
    NSURL *URL = [NSURL URLWithString:@"http://www.google.com"];
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[string, URL]
                                      applicationActivities:nil];
    [self.navigationController presentViewController:activityViewController
                                       animated:YES
                                     completion:^{
                                         // ...
                                     }];}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBActions

- (IBAction)btnCallTapped:(id)sender {
    
/*    if (![UtilityClass getUserInfoDictionary]) {
        
        [UtilityClass showAlertWithTitle:APP_NAME message:@"To use this feature, plesae login first" onViewController:self withButtonsArray:@[@"ok"] dismissBlock:nil];
        return;
    }*/

    NSString *phoneNumber = adminPhoneNumber;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phoneNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)btnEmailTapped:(id)sender {

    if ([MFMailComposeViewController canSendMail]) {
        NSString *strEmailAddress = [NSString stringWithFormat:@"%@",adminEmailAddress];
        // Email Subject
        NSString *emailTitle = @"";
        // Email Content
        NSString *messageBody = @"";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:strEmailAddress];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)btnAppointmentTapped:(id)sender {
 
    AppointmentViewController *pdvc=[self.storyboard instantiateViewControllerWithIdentifier:@"AppointmentViewController"];
    [self.navigationController pushViewController:pdvc animated:YES];
}

#pragma mark - UIScrollView Delegate Method

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger page = round(scrollView.contentOffset.x / self.view.frame.size.width);
    [self.pageControl setCurrentPage:page];
}

- (NSString *)stringByStrippingHTML:(NSString *)inputString
{
    NSMutableString *outString;
    
    if (inputString)
    {
        outString = [[NSMutableString alloc] initWithString:inputString];
        
        if ([inputString length] > 0)
        {
            NSRange r;
            
            while ((r = [outString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                [outString deleteCharactersInRange:r];
            }
        }
    }
    
    return outString;
}
@end
