//
//  LoginVC.m
//  Himilton
//
//  Created by Sumit Sharma on 5/29/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "LoginVC.h"
#import "ForgotPassVC.h"

@import Firebase;
@interface LoginVC ()
{
    
    IBOutlet UITextField *txtFldUsername;
    IBOutlet UITextField *txtFldPassword;
}
@end

@implementation LoginVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [txtFldUsername setValue:[UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [txtFldPassword setValue:[UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1]forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)crossBtnClicked:(id)sender
{
        [self dismissViewControllerAnimated:YES completion:nil];
    self.tabBarController.selectedIndex=0;
}
- (IBAction)forgotPasswordClicked:(UIButton *)sender {
    ForgotPassVC *fpvc=[self.storyboard instantiateViewControllerWithIdentifier:@"forgotPass"];
    [self.navigationController pushViewController:fpvc animated:YES];

}
- (IBAction)loginButtonClicked:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (![UtilityClass isValueNotEmpty:txtFldUsername.text]) {
        [KiplToastView showToastOver:self.view message:@"Please Enter UserName" duration:1.5];
    }
    else if (![UtilityClass isValueNotEmpty:txtFldPassword.text]) {
        [KiplToastView showToastOver:self.view message:@"Please Enter Password" duration:1.5];
    }
    else if (txtFldPassword.text.length<6)
    {
        [KiplToastView showToastOver:self.view message:@"Password Should be atleast 6 Character Long." duration:1.5];
    }
    else
    {
        //Get GFM Refresh Token
        NSString *refreshedToken = [[FIRInstanceID instanceID] token];
        NSLog(@"InstanceID token: %@", refreshedToken);
        
       // [UtilityClass showAlertWithTitle:refreshedToken message:refreshedToken onViewController:self withButtonsArray:nil dismissBlock:nil];
        
        [UtilityClass showHud];
        NSURL *apiURL=[NSURL URLWithString:API_LOGIN];
        NSDictionary *parameter=@{
                                  @"key":SERVICE_KEY,
                                  @"username":txtFldUsername.text,
                                  @"password":txtFldPassword.text,
                                  @"registrationId":refreshedToken
                                  
                                  };
        
        __weak typeof(self)weakSelf = self;
        
        [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
            
            [UtilityClass hideHud];
            if (statusCode==200) {
                
                if ([dictionary[@"status"]boolValue]) {
                    [UtilityClass saveUserInfoWithDictionary:dictionary[@"data"]];
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"msg"] onViewController:self withButtonsArray:@[@"ok"] dismissBlock:^(NSInteger buttonIndex) {
                        
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
                else
                {
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"msg"] onViewController:self withButtonsArray:@[@"ok"] dismissBlock:^(NSInteger buttonIndex) {
                        
                       // [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                }
                
                
                
            }
            else
            {
                if (dictionary[@"reply"]&&[dictionary[@"reply"]isKindOfClass:[NSString class]]) {
                    [UtilityClass showAlertWithTitle:APP_NAME message:dictionary[@"reply"] onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
                }
                else
                {
                    [UtilityClass showAlertWithTitle:APP_NAME message:GLOBAL_ERROR onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
                }
            }
        } failure:^(NSError *error) {
            [UtilityClass hideHud];
            [UtilityClass showAlertWithTitle:APP_NAME message:error.localizedDescription onViewController:weakSelf withButtonsArray:@[@"OK"] dismissBlock:nil];
        }];

    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
