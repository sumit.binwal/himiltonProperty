//
//  SearchViewController.m
//  Himilton
//
//  Created by Rohit Sharma on 6/11/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "SearchViewController.h"
#import "FilterViewController.h"
#import "PropertieVC.h"
#import "OfferViewController.h"

@interface SearchViewController () <FilterViewControllerDelegate,UITextFieldDelegate>

@property (nonatomic, strong) NSMutableDictionary *filters;
@property (strong, nonatomic) IBOutlet UIButton *btnLeft;
@property (strong, nonatomic) IBOutlet UIButton *btnRight;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldSearch;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.filters = [[NSMutableDictionary alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.txtFieldSearch.text=@"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSearchTapped:(id)sender {
}
- (IBAction)btnRightClicked:(UIButton *)sender
{
    [sender setBackgroundImage:[UIImage imageNamed:@"buttonRight"] forState:UIControlStateNormal];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnLeft setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_btnLeft setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.filters setObject:@"Rent" forKey:@"type"];
}

- (IBAction)btnLeftClicked:(UIButton *)sender {
    [sender setBackgroundImage:[UIImage imageNamed:@"buttonLeft"] forState:UIControlStateNormal];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnRight setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [_btnRight setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.filters setObject:@"Buy" forKey:@"type"];
}

- (IBAction)ourOfferBtnAction:(id)sender
{
    OfferViewController *offerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"offerVC"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:offerVC];
    
    [self presentViewController:navController animated:YES completion:nil];
}
#pragma mark - UITextField Delegate Method
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length>0)
    {
        [self.filters setObject:textField.text forKey:@"keywords"];
        [self showFilterScreen];
    }
    
    return YES;
}

- (void)showFilterScreen {
    
    FilterViewController *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:filterVC];
    [filterVC setDelegate:self];
    [filterVC setFilterDetail:self.filters];
    [self presentViewController:navController animated:YES completion:nil];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"showFilterScreenSegue"]) {
        FilterViewController *filterVC = (FilterViewController *)[segue destinationViewController];
        [filterVC setDelegate:self];
        [filterVC setFilterDetail:self.filters];
    }
    else if ([segue.identifier isEqualToString:@"showFilteredPropertyList"]) {
        NSDictionary *dict = (NSDictionary *)sender;
        PropertieVC *propertyVC = (PropertieVC *)[segue destinationViewController];
        [propertyVC setFilterDict:dict];
    }
}

#pragma mark - FilterViewController Delegate

- (void)showResultViewWithFilter:(NSMutableDictionary *)dict {
//    [self performSegueWithIdentifier:@"showFilteredPropertyList" sender:dict];
    [self.tabBarController setSelectedIndex:1];
    UINavigationController *navController = (UINavigationController *)self.tabBarController.viewControllers[1];
    [navController popToRootViewControllerAnimated:NO];
    PropertieVC *propertyVC = navController.viewControllers[0];
    [propertyVC setFilterDict:dict];
}
- (IBAction)searchButtonCLicked:(id)sender
{
    if (self.txtFieldSearch.text.length>0) {
        [self.filters setObject:self.txtFieldSearch.text forKey:@"keywords"];
        [self showFilterScreen];
    }
}

@end
