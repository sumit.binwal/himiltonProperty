//
//  TableContentCell.m
//  Himilton
//
//  Created by Sumit Sharma on 6/15/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "TableContentCell.h"

@implementation TableContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
