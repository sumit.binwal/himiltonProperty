//
//  TabIndexCollectionCell.h
//  Himilton
//
//  Created by Sumit Sharma on 6/15/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabIndexCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTabTitle;
@property (strong, nonatomic) IBOutlet UIView *vwBottom;

@end
