//
//  TableContentCell.h
//  Himilton
//
//  Created by Sumit Sharma on 6/15/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableContentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UILabel *labelEmailAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

@property (strong, nonatomic) IBOutlet UIButton *btnCC1;
@property (strong, nonatomic) IBOutlet UIButton *btnCC2;
@property (strong, nonatomic) IBOutlet UILabel *labelCCSupervisorName;
@property (strong, nonatomic) IBOutlet UILabel *labelCCSupervisorEmail;
@property (strong, nonatomic) IBOutlet UILabel *labelCCSupervisorPhone;

@end
