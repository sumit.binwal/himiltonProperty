//
//  OfferCustomeCell.h
//  Himilton
//
//  Created by Sumit Sharma on 6/22/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferCustomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewOffer;
@property (strong, nonatomic) IBOutlet UILabel *labelOfferTitle;

@end
