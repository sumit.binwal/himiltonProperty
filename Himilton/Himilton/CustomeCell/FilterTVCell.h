//
//  FilterTVCell.h
//  Himilton
//
//  Created by Rohit Sharma on 5/31/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FilterCellType) {
    StaticLabel = 0,
    Option,
    Nearby,
    Keyword
};

@protocol FilterTVCellDelegate <NSObject>

- (void)updateValueForKey:(NSString *)item withValue:(NSString *)filterValue;

@end

@interface FilterTVCell : UITableViewCell

@property (nonatomic, assign) id<FilterTVCellDelegate> delegate;
@property (nonatomic, assign) FilterCellType cellType;
- (void)setUpUIForDict:(NSArray *)filters andFilterName:(NSString *)filterName;
- (void)updatePriceOrPropertyValue:(NSString *)value;
@end
