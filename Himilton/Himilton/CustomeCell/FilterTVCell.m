//
//  FilterTVCell.m
//  Himilton
//
//  Created by Sumit Sharma on 5/31/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "FilterTVCell.h"
#import "AutoLayoutHelpers.h"

@interface FilterTVCell () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *nearbyView;
@property (weak, nonatomic) IBOutlet UIView *keywordsView;
@property (weak, nonatomic) IBOutlet UILabel *lblNearby;
@property (weak, nonatomic) IBOutlet UISwitch *swtchNearby;
@property (weak, nonatomic) IBOutlet UILabel *lblKeyword;
@property (weak, nonatomic) IBOutlet UITextField *txtfKeyword;

@property (weak, nonatomic) IBOutlet UIView *staticLabelView;
@property (weak, nonatomic) IBOutlet UIView *optionsView;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceProperty;
@property (weak, nonatomic) IBOutlet UILabel *lblPricePropertyValue;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionsType;
@property (weak, nonatomic) IBOutlet UIView *viewOptionsContainer;

@property (nonatomic, strong) UISegmentedControl *sgmtControl;
@property (nonatomic, strong) NSArray *filters;
@property (nonatomic, strong) NSString *filterName;
@end

@implementation FilterTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setUpUIForDict:(NSArray *)filters andFilterName:(NSString *)filterName {
    self.filters = filters;
    self.filterName = filterName;
    
    if (self.cellType == StaticLabel) {
        self.staticLabelView.hidden = false;
        self.optionsView.hidden = true;
        self.nearbyView.hidden = true;
        self.keywordsView.hidden = true;
        
        [self.lblPriceProperty setText:filterName];
        [self.lblPricePropertyValue setText:self.filters[0]];
        [self callDelegateMethodWithKey:filterName andValue:self.filters[0]];
    } else if (self.cellType == Option) {
        self.staticLabelView.hidden = true;
        self.optionsView.hidden = false;
        self.nearbyView.hidden = true;
        self.keywordsView.hidden = true;
        
        [self.lblOptionsType setText:filterName];
        
        NSMutableArray *arrFilter = [NSMutableArray arrayWithArray:self.filters];
        [arrFilter insertObject:@"Any" atIndex:0];
        if (arrFilter.count >= 6) {
            NSString *strFourthObject = arrFilter[5];
            strFourthObject = [strFourthObject stringByAppendingString:@"+"];
            arrFilter[5] = strFourthObject;
            
            NSInteger count = arrFilter.count - 5;
            NSIndexSet *indexSet;
            for (NSUInteger i = 6; i < arrFilter.count; i++) {
                NSRange range = NSMakeRange(6, count);
                indexSet = [NSIndexSet indexSetWithIndexesInRange:range];
            }
        }
        self.filters = arrFilter;
        
        [self createSegmentControl];
        [self callDelegateMethodWithKey:filterName andValue:self.filters[0]];
    } else if (self.cellType == Nearby) {
        self.staticLabelView.hidden = true;
        self.optionsView.hidden = true;
        self.nearbyView.hidden = false;
        self.keywordsView.hidden = true;
        
        [self.lblNearby setText:filterName];
        
    } else if (self.cellType == Keyword) {
        self.staticLabelView.hidden = true;
        self.optionsView.hidden = true;
        self.nearbyView.hidden = true;
        self.keywordsView.hidden = false;
        
        [self.lblKeyword setText:filterName];
        
    }
    
}

- (void)callDelegateMethodWithKey:(NSString *)key andValue:(NSString *)value {
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateValueForKey:withValue:)]) {
        [self.delegate updateValueForKey:key withValue:value];
    }
}

- (void)createSegmentControl {
    
    
    self.sgmtControl = [[UISegmentedControl alloc] initWithItems:self.filters];
    [self.sgmtControl setTintColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0]];
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[[UIColor blackColor]] forKeys:@[NSForegroundColorAttributeName]];
    [self.sgmtControl setTitleTextAttributes:dict forState:UIControlStateNormal];
    NSDictionary *selectedDict = [NSDictionary dictionaryWithObjects:@[[UIColor whiteColor]] forKeys:@[NSForegroundColorAttributeName]];
    [self.sgmtControl setTitleTextAttributes:selectedDict forState:UIControlStateSelected];
    [self.sgmtControl setSelectedSegmentIndex:0];
    [self.sgmtControl addTarget:self action:@selector(segmentControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.sgmtControl setBackgroundImage:[UIImage imageNamed:@"filter_select_box_empty"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.sgmtControl setBackgroundImage:[UIImage imageNamed:@"filter_select_box_color"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    [self.sgmtControl.layer setBorderColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0].CGColor];
    [self.sgmtControl.layer setBorderWidth:1.0];
    [self.viewOptionsContainer addSubview:self.sgmtControl];
    
    CGFloat sgmtControlWidth = self.bounds.size.width - 20;
    CGFloat width = sgmtControlWidth / self.filters.count;
    for (int i = 0; i < self.filters.count; i++) {
        [self.sgmtControl setWidth:width forSegmentAtIndex:i];
        
    }
    
    [self.viewOptionsContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    constraintEqual(self.sgmtControl, self.viewOptionsContainer, NSLayoutAttributeTop, 0.0);
    constraintEqual(self.sgmtControl, self.viewOptionsContainer, NSLayoutAttributeBottom, 0.0);
    constraintEqual(self.sgmtControl, self.viewOptionsContainer, NSLayoutAttributeLeading, 0.0);
    constraintEqual(self.sgmtControl, self.viewOptionsContainer, NSLayoutAttributeTrailing, 0.0);
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    CGRect bounds = CGRectMake(0.0, 0.0, self.sgmtControl.frame.size.width - 10, self.sgmtControl.bounds.size.height);
    [maskLayer setFrame:bounds];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(20.0, 20.0)];
    maskLayer.path = path.CGPath;
    [self.sgmtControl.layer setMask:maskLayer];
    
    CAShapeLayer *borderLayer = [CAShapeLayer layer];
    borderLayer.path = path.CGPath;
    [borderLayer setLineWidth:2.0];
    [borderLayer setFrame:bounds];
    [borderLayer setFillColor:[UIColor clearColor].CGColor];
    [borderLayer setStrokeColor:[UIColor colorWithRed:243/255.0 green:117/255.0 blue:34/255.0 alpha:1.0].CGColor];
    [self.sgmtControl.layer addSublayer:borderLayer];
    [self.sgmtControl.layer setMasksToBounds:false];
}

- (void)updatePriceOrPropertyValue:(NSString *)value {
    [self.lblPricePropertyValue setText:value];
    [self callDelegateMethodWithKey:self.filterName andValue:value];
}

- (void)segmentControlValueChanged:(UISegmentedControl *)sender {
    NSInteger segmentIndex = [self.sgmtControl selectedSegmentIndex];
    NSString *value = [self.sgmtControl titleForSegmentAtIndex:segmentIndex];// [self.filters objectAtIndex:segmentIndex];
    
    [self callDelegateMethodWithKey:self.filterName andValue:value];
}

- (IBAction)swtchNearbyValueChanged:(id)sender {
    if (self.swtchNearby.isOn) {
        [self callDelegateMethodWithKey:self.filterName andValue:@"10"];
    }else {
        [self callDelegateMethodWithKey:self.filterName andValue:@""];
    }
}

#pragma mark - TextField Delegate Method

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField isEqual:self.txtfKeyword]) {
        [self callDelegateMethodWithKey:self.filterName andValue:textField.text];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
