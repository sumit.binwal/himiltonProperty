//
//  PropertieListCell.h
//  Himilton
//
//  Created by Sumit Sharma on 5/29/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PropertieListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVwPropertie;
@property (strong, nonatomic) IBOutlet UIButton *buttonImageLike;
@property (strong, nonatomic) IBOutlet UILabel *labelBed;
@property (strong, nonatomic) IBOutlet UILabel *labelBath;
@property (strong, nonatomic) IBOutlet UILabel *labelCar;
@property (strong, nonatomic) IBOutlet UILabel *labelAddress;
@property (strong, nonatomic) IBOutlet UILabel *labelPrice;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwLike;
@property (strong, nonatomic) IBOutlet UIButton *btnOurOffer;

@end
