//
//  AppDelegate.m
//  Himilton
//
//  Created by Sumit Sharma on 5/24/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import "AppDelegate.h"
#import <IQKeyboardManager.h>
#import <UserNotifications/UserNotifications.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@import Firebase;

CGFloat scaleFactorX;
CGFloat scaleFactorY;
NSString *adminPhoneNumber;
NSString *adminEmailAddress;
NSString *notificationFlag;
NSUserDefaults *userDefaults;
@interface AppDelegate ()<UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    scaleFactorX=[UIScreen mainScreen].bounds.size.width/375;
    scaleFactorY=[UIScreen mainScreen].bounds.size.height/667;
    userDefaults=[NSUserDefaults standardUserDefaults];
    
    [Fabric with:@[[Crashlytics class]]];

    [GMSServices provideAPIKey:@"AIzaSyCoVxpViA3luKBVjU4rAS8eIODnK-VyFEw"];

    
    [self getAdminContectDetail];
    //IQKeyboard Methods
    [[IQKeyboardManager sharedManager]setEnable:YES];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField=100.0f;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside=YES;

    [[UINavigationBar appearance]setBackgroundColor:[UIColor colorWithRed:233.0f/255.0f green:233.0f/255.0f blue:233.0f/255.0f alpha:1]];
    [[UINavigationBar appearance]setTintColor:[UIColor blackColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 data message (sent via FCM)
        [FIRMessaging messaging].remoteMessageDelegate = self;
        
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    // [START add_token_refresh_observer]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    

    
    return YES;
}

-(void)getAdminContectDetail
{
    NSURL *apiURL;
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
        apiURL=[NSURL URLWithString:API_GET_ADMIN_DETAIL];
    
        [parameter setObject:SERVICE_KEY forKey:@"key"];
    
    [[AppWebHandler sharedInstance]fetchDataFromUrl:apiURL httpMethod:HttpMethodTypePost parameters:parameter shouldDeSerialize:YES success:^(NSData *data, NSDictionary *dictionary, NSInteger statusCode) {
        
        if (statusCode==200) {
            adminPhoneNumber=[NSString stringWithFormat:@"%@",[dictionary[@"data"]objectAtIndex:0][@"adminphone"]];
            adminEmailAddress=[NSString stringWithFormat:@"%@",[dictionary[@"data"]objectAtIndex:0][@"adminemail"]];
        }
        
    } failure:^(NSError *error) {
       
    }];

}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs token here.
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
}

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
            NSString *refreshedToken = [[FIRInstanceID instanceID] token];
            NSLog(@"InstanceID token: %@", refreshedToken);
        }
    }];
}
// [END connect_to_fcm]

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print full message.
    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [END receive_message]




// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    
    
    NSDictionary *userInfo = notification.request.content.userInfo;
//    if ([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[RESideMenuViewController class]])
//    {
//        RESideMenuViewController *controller = (RESideMenuViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    
//        if ([controller.contentViewController isKindOfClass:[UINavigationController class]])
//        {
//            UINavigationController *centerNavigation=(UINavigationController *)controller.contentViewController;
//            
//            
//            if ([[centerNavigation.viewControllers lastObject] isKindOfClass:[JobsVC class]]) {
//                
//                JobsVC *topController = (JobsVC*)[centerNavigation.viewControllers lastObject];
//                
//                if(topController.selectedTabIndex == 0)
//                {
//                    if ([userInfo[@"activity"]isEqualToString:@"on_decline_user"]||[userInfo[@"activity"]isEqualToString:@"on_accept"]||[userInfo[@"activity"]isEqualToString:@"on_reject"]) {
//                        
//                        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_ISREQUESTACCEPTBYDRIVER object:nil];
//                        
//                    }
//                }
//            }
//        }
        
  //  }
    
    
    
    //[userDefaults setBool:true forKey:NOTIFICATION_ISREAD];
    //[[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_ISREAD object:nil];
    
    // Print full message.
    //NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    // Print full message.
    NSLog(@"%@", userInfo);
  
    notificationFlag=userInfo[@"userinfo"];
   UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    NSLog(@"%@",tabBarController.viewControllers);
    tabBarController.selectedIndex=3;
   // NotificationVC *carriersVC = [[UIStoryboard getNotificationStoryboard]instantiateInitialViewController];
    
    
   // UINavigationController *navController = (UINavigationController *)((RESideMenuViewController*)self.window.rootViewController).contentViewController;
    
   // [navController pushViewController:carriersVC animated:YES];
    
    completionHandler();
}
// [END ios_10_message_handling]

@end
