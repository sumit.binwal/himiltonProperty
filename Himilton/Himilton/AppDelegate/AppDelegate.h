//
//  AppDelegate.h
//  Himilton
//
//  Created by Sumit Sharma on 5/24/17.
//  Copyright © 2017 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic)IBOutlet UIWindow *window;

FOUNDATION_EXPORT CGFloat scaleFactorX;
FOUNDATION_EXPORT CGFloat scaleFactorY;

FOUNDATION_EXPORT NSString *adminPhoneNumber;
FOUNDATION_EXPORT NSString *adminEmailAddress;

FOUNDATION_EXPORT NSString *notificationFlag;


FOUNDATION_EXPORT NSUserDefaults *userDefaults;
@end

